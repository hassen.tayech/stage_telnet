import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminComponent} from './layout/admin/admin.component';
import {AuthComponent} from './layout/auth/auth.component';
import { AuthGuard } from 'app/guards';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { HassenComponent } from './hassen/hassen.component';
//import { DatepickerComponent } from './hassen/datepicker/datepicker.component';

const routes: Routes = [
  // { path : 'hassen' , component : HassenComponent },

  {
    path: '',
    component: AuthComponent,
    
    children: [
       {
         path: '',
         redirectTo: 'auth/login',
         pathMatch: 'full'
       },
      {
        path: 'auth',
        loadChildren: './components/auth/auth.module#AuthModule'
      },
      {
        path: 'page-not-found', component: PageNotFoundComponent
      },
    ]
  },
  {
    path: '',
    component: AdminComponent,
   // canActivate: [AuthGuard],
    //canActivateChild: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        loadChildren: './components/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'manageProjects',
        loadChildren: './components/manageProjects/manageProject.module#ManageProjectModule'
      },
      { path : 'hassen' , component : HassenComponent }
 
    ]
  }//,
  //{
  //  path:'**', redirectTo: 'page-not-found'
  //}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
