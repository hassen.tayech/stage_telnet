import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TimeSheetComponent } from 'app/components/manageProjects/timeSheets/timeSheet.component';
import { ProjectIndexComponent } from 'app/components/manageProjects/Projects/index.component';
import { CreateProjectComponent } from './Projects/create/createProject.component';
import { ViewNewProjectsComponent} from './Projects/view-new/viewNewProjects.component';
import { ViewAllProjectsComponent } from './Projects/view-all/viewAllProjects.component';
import { EditProjectComponent } from './Projects/edit/editProject.component';
import { CanDeactivateGuard } from 'app/guards/can-deactivate-guard';
import { PifResolver, PifDDListResolver } from 'app/resolvers/pif-resolver.service';
import { ProjectResolver, ProjectDDListResolver } from 'app/resolvers/project-resolver.service';

export const ManageProjectRoutes: Routes = [
  {
    path: '',
    data: {
      title: 'Projets Management'
    },
    children: [
      {
        path: 'timesheet',
        component: TimeSheetComponent,
        data: {
          title: 'TimeSheet'
        }
      },
      {
        path: 'projects', component: ProjectIndexComponent,
        data: {
          title: 'Projects'
        },
        children: [
          { path: '', redirectTo: 'new-projects',pathMatch: 'full'  },
          { 
            path: 'create',
            component: CreateProjectComponent,
            canDeactivate: [CanDeactivateGuard],
            resolve: {
              ddList: PifDDListResolver
            }
          },
          { 
            path: 'create/:id',
            component: CreateProjectComponent,
            canDeactivate: [CanDeactivateGuard],
            resolve: {
                pif: PifResolver,
                ddList: PifDDListResolver
            }
          },
          { 
            path: 'edit/:id',
            component: EditProjectComponent,
            canDeactivate: [CanDeactivateGuard],
            resolve: {
              project: ProjectResolver,
              ddList: ProjectDDListResolver
            }
          },
          { path: 'new-projects', component: ViewNewProjectsComponent },
          { path: 'all-projects', component: ViewAllProjectsComponent }
        ]
      }
    ]
  }
];


