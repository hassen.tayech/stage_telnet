import { Component, Inject, NgModule} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { userProject } from 'app/models/manageProject/timesheet';
import { ProjectService} from 'app/services';
import { User } from 'app/models/user';
import { userActivity } from 'app/models/manageProject/timesheet';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'ProjectList_component',
  templateUrl: './ProjectList.component.html',
  styleUrls: ['./ProjectList.component.scss']
})

export class ProjectListComponent {
  userActivityList: userActivity[];

  userProjectList: userProject[];

  public currentUser: User;

  constructor(private dialogRef: MatDialogRef<ProjectListComponent>, private projectService: ProjectService, @Inject(MAT_DIALOG_DATA) private data) 
    {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.userProjectList = this.data.userProjectList;
    }
  
  ngOnInit() { }

 
  close() {
    this.dialogRef.close();
  }

  expandCollapse(element){
    var parentElement = element.currentTarget.parentNode.parentNode.parentNode.getElementsByClassName("collapsible");
    var currentElement = element.currentTarget;
    for (var index = 0; index < parentElement.length; index++) {
      if (parentElement[index].hidden) {
        parentElement[index].removeAttribute("hidden");
        currentElement.innerText = "keyboard_arrow_up";
      } else {
        parentElement[index].hidden = "true";
        currentElement.innerText = "keyboard_arrow_down";
      }
    }
  }
  
  CreateList(){
    var userProjectListSelected: userProject[] = [];

    this.userProjectList.forEach(projectItem => {
      let activitiesSelected = projectItem.projectActivities.filter(a => a.ACTIVITY_SELECTED===true);
      if(activitiesSelected != undefined && activitiesSelected.length > 0) 
      {
        // activitiesSelected
        //   .forEach( item => {
        //     if(item.userTimeEntry == null && item.userTimeEntry == undefined)
        //       item.userTimeEntry = new weektimeEntry() ;
        //   });
        let project = new userProject();
        project.PROJECT_ID = projectItem.PROJECT_ID;
        project.PROJECT_NAME = projectItem.PROJECT_NAME;
        project.projectActivities.push(...activitiesSelected);
        userProjectListSelected.push(project);
        this.projectService.refreshActivitiesSelected.emit(userProjectListSelected);
      }
    })
    this.dialogRef.close();
  }

  selectAllActivities(projectId, event){
    this.userProjectList.forEach(projectItem => {
      if(projectItem.PROJECT_ID === projectId) 
      {
        if(event === true)
          { projectItem.SELECT_DESELECT_PROJECT = true;
            projectItem.projectActivities.forEach(activitiesItem => { activitiesItem.ACTIVITY_SELECTED = true });}
        else {
          projectItem.SELECT_DESELECT_PROJECT = false;
          projectItem.projectActivities.forEach(activitiesItem => { activitiesItem.ACTIVITY_SELECTED = false });
        }
      }
    })
  }

  selectDeselect(projectId, event){
    this.userProjectList.forEach(projectItem => {
      if(projectItem.PROJECT_ID === projectId) 
      {
        if(!event) 
        {
          projectItem.SELECT_DESELECT_PROJECT = false;
        }
      }
    })
  }

}
