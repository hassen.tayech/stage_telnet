import { Component } from '@angular/core';
//import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
export interface AddNewTaskModel {
  title: string;
  message: string;
}
@Component({
  selector: 'AddNewTask',
  templateUrl: './AddNewTask.component.html'
})
//export class AddNewTaskComponent extends DialogComponent<AddNewTaskModel, boolean> implements AddNewTaskModel {
  export class AddNewTaskComponent{
  title: string;
  message: string;
  //constructor(dialogService: DialogService) {
  //  super(dialogService);
 // }
  confirm() {
    // we set dialog result as true on click on confirm button, 
    // then we can get dialog result from caller code 
    //this.result = true;
    //this.close();
  }
}
