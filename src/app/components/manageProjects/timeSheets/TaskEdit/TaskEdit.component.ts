import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Component({
  selector: 'TaskEdit_component',
  templateUrl: './TaskEdit.component.html',
  styleUrls: ['./TaskEdit.component.scss']
})

export class TaskEditComponent {

  taskName: string;

  constructor(private dialogRef: MatDialogRef<TaskEditComponent>, @Inject(MAT_DIALOG_DATA) private data) {
    this.taskName = this.data.taskName;
  }

  close() {
    this.dialogRef.close();
  }

}
