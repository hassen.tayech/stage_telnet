
import { Component, Input } from '@angular/core';
import { AddNewTaskComponent } from 'app/components/manageProjects/timeSheets/AddNewTask.component';
import * as moment from 'moment';
import { User } from 'app/models/user';
import { MatDialog, MatDialogConfig, MatDialogRef} from "@angular/material";
import { TaskEditComponent } from './TaskEdit/TaskEdit.component';
import { ProjectListComponent } from './ProjectList/ProjectList.component';
import { ProjectService, DateTimeService} from 'app/services';
import { ActivatedRoute } from '@angular/router';
// import { userProject, timeEntry, weektimeEntry } from 'app/models/manageProject/timesheet';
import { error } from 'util';
import { userProject } from 'app/models/manageProject/timesheet';


@Component({
  selector: 'TimeSheet-component',
  templateUrl: './timeSheet.view.html',
  styleUrls: ['./timeSheet.component.scss']
})
export class TimeSheetComponent {  
  userProjectListSelected: userProject[];
  userProjectList: userProject[];
  currentUser: User;
  days = [];
  DisplayTaskEditdialogRef: MatDialogRef<TaskEditComponent>;
  DisplayProjectListdialogRef: MatDialogRef<ProjectListComponent>;

  startOfWeek = moment().startOf('isoWeek');
  endOfWeek = moment().endOf('isoWeek');

  constructor(private dialog: MatDialog,
              private projectService: ProjectService,
              private _avRoute: ActivatedRoute,
              private dateTimeService: DateTimeService) {

    // this.projectService.refreshActivitiesSelected
    //       .subscribe(
    //           (status: userProject[]) => {
    //               this.userProjectListSelected = status;
    //               console.log(this.userProjectListSelected);
    //           });
    
    }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.GetUserActivity(+this.currentUser.UserId);
    this.days = this.dateTimeService.getCurrentWeekDays();
    this.dateTimeService.getCurrentWeekDays();
  }

  startOfCurrentDate= moment().startOf('week').add(1, 'days').format("L").toString();  
  endOfCurrentDate= moment().startOf('week').add(7, 'days').format("L").toString(); 


  expandCollapse(element){
    var parentElement = element.currentTarget.parentNode.parentNode.parentNode.getElementsByClassName("collapsible");
    var currentElement = element.currentTarget;
    for (var index = 0; index < parentElement.length; index++) {
      if (parentElement[index].hidden) {
        parentElement[index].removeAttribute("hidden");
        currentElement.innerText = "keyboard_arrow_up";
      } else {
        parentElement[index].hidden = "true";
        currentElement.innerText = "keyboard_arrow_down";
      }
    }
  }
  
  prev(){
    //this.days =  this.dateTimeService.getPreviousWeek(this.days[0], this.days[6]);
  }

  next(){
    //this.days =  this.dateTimeService.getNextWeek(this.days[0], this.days[6]);
  }

  openDisplayDialog(taskName) {
    let config: MatDialogConfig = {
      disableClose: false,
      panelClass: "form_modal",
      hasBackdrop: true,
      data: { 
        taskName: taskName,
      }
    };

    this.DisplayTaskEditdialogRef = this.dialog.open(TaskEditComponent, config);
  }

  openProjectListDialog() {
    let config: MatDialogConfig = {
      disableClose: false,
      panelClass: "form_modal",
      hasBackdrop: true,
      data: { 
        userProjectList: this.userProjectList,
        
      }
    };

    this.DisplayProjectListdialogRef = this.dialog.open(ProjectListComponent, config);
  }


  GetUserActivity(userId: number) {
    this.projectService.GetUserActivity(userId)
      .subscribe(
        data => {
          this.userProjectList = data;

        },
        error => {

        }
      );
       
  }

}


