import { Component, OnInit, AfterViewInit, Renderer } from '@angular/core';
import { error } from 'selenium-webdriver';

import { PifSearchResponse, } from 'app/models/manageProject';
import { User } from 'app/models/user';
import { DataTableResponse } from 'app/models/shared';
import { ProjectService, NotificationService } from 'app/services';
import { RouterLink, Router, ActivatedRoute } from '@angular/router';


declare const $: any;
declare const that: any;

@Component({
  selector: 'new-projects',
  templateUrl: './viewNewProjects.component.html',
  styleUrls: ['./viewNewProjects.component.scss']
})
export class ViewNewProjectsComponent implements OnInit{

  loggedUser: User;

  //status label colors
  warning: string="#ffc107";
  danger: string="#dc3545";
  pink: string="#e83e8c";
  sucess: string="#28a745";
  info: string="#17a2b8";

  constructor(
    private projectService: ProjectService,
    private notificationService: NotificationService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private renderer: Renderer
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    const that = this;
    $('#PifDatatable').DataTable({
      'pagingType': 'full_numbers',
      responsive: true,
      serverSide: true,
      language: {
        emptyTable: "No data found",
        zeroRecords :"No data found"
      },
      columns: [
        { "data": "ID_PIF", "visible": false},
        { "data": "PROJECT_NAME", "title" :"Project Name"},
        { "data": "ACTIVITY_NAME", "title":"Activity Name" },
        { "data": "PM_NAME", "title":"Created By" },
        { "data": "DH_NAME", "title":"Depart. Header" },
        { "data": "SQA_NAME", "title":"SQA" },
        { "data": "PIF_STATUS",
          "title":"Status",
          "render": function (data) { return that.GetPifStatusRender(data); }
        },
        { "data":"",
          "title":"Action",
          "orderable":false,
          "width":"10px",
          "render":function (data, type, row) { return that.GetPifActionsRender(row.ID_PIF); }
        }
      ],
      ajax: (dataTablesParameters: any, callback) => {
        that.GetUserPifList(dataTablesParameters, callback)
      }
    });

    this.renderer.listenGlobal('document', 'click', (event) => {
      if (event.target.hasAttribute("edit-pif-id")) {
        this.router.navigate(["../create/"+ event.target.getAttribute("edit-pif-id")], {relativeTo:this.activeRoute});
      }
    });
  }

  GetUserPifList(dataTablesParameters: any, callback){
    this.loggedUser = JSON.parse(localStorage.getItem('currentUser'));
    this.projectService.GetUserPifList(this.loggedUser.UserId, dataTablesParameters).subscribe(
      resp => {
        callback({
            recordsTotal: resp.recordsTotal,
            recordsFiltered: resp.recordsFiltered,
            draw: resp.draw,
            data: resp.data
        });
      },
      error => {
        this.notificationService.danger(error);
      }
    );
  };

  GetPifActionsRender(pif_id: string) : string {
    return    '<div style="text-align: center;">'
            +   '<i class=\'icofont icofont-ui-edit\' style="cursor: pointer" edit-pif-id="' + pif_id + '"></i>'
            + '</div>';
  }


  GetPifStatusRender(data: any) : string {
    switch (data)
      {
        case "Creation in progress":
          return this.FormatPifStatus(data, this.info);
        case "Waiting approval":
          return this.FormatPifStatus(data, this.warning);
        case "Approved":
          return this.FormatPifStatus(data, this.sucess);
      }
  }

  FormatPifStatus(data: any, color : string): string {
    return '<span style="background-color:'+ color +' ; padding: 3px; color: white">' + data + '</span>';
  }
}
