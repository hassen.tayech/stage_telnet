import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAllProjectsComponent } from './viewAllProjects.component';

describe('ListComponent', () => {
  let component: ViewAllProjectsComponent;
  let fixture: ComponentFixture<ViewAllProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAllProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAllProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
