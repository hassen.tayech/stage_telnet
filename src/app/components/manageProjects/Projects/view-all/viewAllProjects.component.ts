import { Component, OnInit, AfterViewInit, Renderer } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ProjectService, NotificationService, DateTimeService } from 'app/services';
import { User } from 'app/models/user';
import { ProjectSearchResponse } from 'app/models/manageProject/project-search-response.model';


declare const $: any;
declare const that: any;

@Component({
  selector: 'all-projects',
  templateUrl: './viewAllProjects.component.html',
  styleUrls: ['./viewAllProjects.component.scss']
})
export class ViewAllProjectsComponent implements OnInit {
  loggedUser: User;

  userProjectsShown: boolean;

  //status label colors
  warning: string="#ffc107";
  danger: string="#dc3545";
  pink: string="#e83e8c";
  sucess: string="#28a745";
  info: string="#17a2b8";

  constructor(
    private projectService: ProjectService,
    private notificationService: NotificationService,
    private dateTimeService : DateTimeService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private renderer: Renderer
  )
  {
    this.userProjectsShown = true;
  }

  ngOnInit() {

  }
  
  ShowUserProjects(){
    this.userProjectsShown = true;
    var table = $('#ProjectListDatatable').DataTable();
    table.ajax.reload();
  }

  ShowAllProjects(){
    this.userProjectsShown = false;
    var table = $('#ProjectListDatatable').DataTable();
    table.ajax.reload();
  }

  ngAfterViewInit() {
    const that = this;
    $('#ProjectListDatatable').DataTable({
    //   initComplete: function () {
    //     this.api().columns().every( function () {
    //         var column = this;
    //         var select = $('<select><option value=""></option></select>')
    //             .appendTo( $(column.header()).empty() )
    //             .on( 'change', function () {
    //                 var val = $.fn.dataTable.util.escapeRegex(
    //                     $(this).val()
    //                 );

    //                 column
    //                     .search( val ? '^'+val+'$' : '', true, false )
    //                     .draw();
    //             } );

    //         column.data().unique().sort().each( function ( d, j ) {
    //             select.append( '<option value="'+d+'">'+d+'</option>' )
    //         } );
    //     } );
    // },
      'pagingType': 'full_numbers',
      responsive: true,
      serverSide: true,
      language: {
        emptyTable: "No data found",
        zeroRecords :"No data found"
      },
      columns: [
        { "data": "PROJECT_ID", "visible": false},
        { "data": "PROJECT_NAME", "title" :"Project Name"},
        { "data": "PROJECT_ABBREVIATION", "title" :"Project Abrev"},
        { "data": "ROJECT_REALSTARTDATE",
          "title":"Start Date",
          "render" : function (data){ return that.dateTimeService.getShortFormat(data); }
        },
        { "data": "PROJECT_REALENDDATE",
          "title":"End Date",
          "render" : function (data){ return that.dateTimeService.getShortFormat(data); }
        },
        { "data": "PROJECT_COST", "title":"Cost" },
        { "data": "PROJECT_REFERENCE", "title":"Project Reference" },
        { "data": "ACTIVITY_NAME", "title":"Activity" },
        { "data": "STATUS",
          "title":"Status",
          "render": function (data) { return that.GetProjectStatusRender(data); }
        },
        { "data":"",
          "title":"Action",
          "orderable":false,
          "width":"10px",
          "render":function (data, type, row) { return that.GetProjectActionsRender(row.PROJECT_ID); }
        }
      ],
      "order": [[3, 'desc']],
      ajax: (dataTablesParameters: any, callback) => {
        if (that.userProjectsShown)
          that.GetUserProjectsList(dataTablesParameters, callback);
        else
          that.GetAllProjectsList(dataTablesParameters, callback);
      }
    }); 
    this.renderer.listenGlobal('document', 'click', (event) => {
      if (event.target.hasAttribute("edit-project-id")) {
        this.router.navigate(["../edit/"+ event.target.getAttribute("edit-project-id")], {relativeTo:this.activeRoute});
      }
    });
  }

  GetUserProjectsList(dataTablesParameters: any, callback){
    this.loggedUser = JSON.parse(localStorage.getItem('currentUser'));
    this.projectService.GetUserProjectsList(this.loggedUser.UserId, dataTablesParameters).subscribe(
      resp => {
        callback({
            recordsTotal: resp.recordsTotal,
            recordsFiltered: resp.recordsFiltered,
            draw: resp.draw,
            data: resp.data
        });
      },
      error => {
        this.notificationService.danger(error);
      }
    );
  };

  GetAllProjectsList(dataTablesParameters: any, callback){
    this.projectService.GetAllProjectsList(dataTablesParameters).subscribe(
      resp => {
        callback({
            recordsTotal: resp.recordsTotal,
            recordsFiltered: resp.recordsFiltered,
            draw: resp.draw,
            data: resp.data
        });
      },
      error => {
        this.notificationService.danger(error);
      }
    );
  };
  
  GetProjectActionsRender(project_id: string) : string {
    return    '<div style="text-align: center;">'
            +   '<i class=\'icofont icofont-ui-edit\' style="cursor: pointer" edit-project-id="' + project_id + '"></i>'
            + '</div>';
  }
  GetProjectStatusRender(data: any) : string {
    switch (data)
      {
        case "In progress":
          return this.FormatProjectStatus(data, this.sucess)
        case "Blocked":
          return this.FormatProjectStatus(data, this.danger)
        case "Closed":
          return this.FormatProjectStatus(data, this.pink)
        case "Planned":
          return this.FormatProjectStatus(data, this.info)
        case "Finished":
          return this.FormatProjectStatus(data, this.warning)
      }
  }

  FormatProjectStatus(data: any, color : string): string {
    return '<span style="background-color:'+ color +' ; padding: 3px; color: white">' + data + '</span>';
  }
}


