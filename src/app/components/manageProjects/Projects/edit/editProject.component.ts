import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, Data } from '@angular/router';
//import { FormControl } from '@angular/forms';
import { ProjectService, NotificationService } from 'app/services';
import { DDListModel } from 'app/models/manageProject/DDList.model';
import { ProjectModel } from 'app/models/manageProject';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { map } from 'rxjs/operators/map';
import { Observable } from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {animate, style, transition, trigger} from '@angular/animations';
import {FormControl, Validators, NgForm} from '@angular/forms';
import { CanComponentDeactivate } from 'app/guards/can-deactivate-guard';


@Component({
  selector: 'app-add',

  templateUrl: './editProject.component.html',
  styleUrls: ['./editProject.component.scss'],
  animations: [
          trigger('fadeInOutTranslate', [
            transition(':enter', [
              style({opacity: 0}),
              animate('400ms ease-in-out', style({opacity: 1}))
            ]),
            transition(':leave', [
              style({transform: 'translate(0)'}),
              animate('400ms ease-in-out', style({opacity: 0}))
            ])
          ])
        ]
})

export class EditProjectComponent implements OnInit, CanComponentDeactivate {
  public user: any;
  public project: ProjectModel = new ProjectModel();
  public DDLists: DDListModel[];
  public ClientDDList: DDListModel[];
  public CategoryDDList: DDListModel[];
  public ActivityDDList: DDListModel[];
  public TechnoDDList: DDListModel[];
  public StatusDDList: DDListModel[];
  disableEdit : boolean = false;
  @ViewChild('techSelect') groupsSelect: any;
  @ViewChild('projectDetailsForm') detailsForm : NgForm;

  constructor(
    private _avRoute: ActivatedRoute,
    private _router: Router,
    private projectService: ProjectService,
    private notificationService: NotificationService) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentUser'));

    //resolvers used to load project details and dd list content before loading the componenet.
    this._avRoute.data
    .subscribe(
      (data: Data) => {
        if (data['project'] != null)
        {
          this.project = data['project'];
          this.groupsSelect.value = this.project.TechIds;
          //this.disableEdit = this.project.PIF_STATUS != 'Creation in progress';
        };

        if (data['ddList'] != null && data['ddList'] != undefined)
        {
          this.ActivityDDList = data['ddList'].filter(act => act.CODE_NAME === 'SI_ACTIVITY');
          this.ClientDDList = data['ddList'].filter(cli => cli.CODE_NAME === 'GP_CLIENT');
          this.CategoryDDList = data['ddList'].filter(cat => cat.CODE_NAME === 'GP_PROJECT_CATEGORY');
          this.TechnoDDList = data['ddList'].filter(tech => tech.CODE_NAME === 'GP_FUNCTIONPOINT');
          this.StatusDDList = data['ddList'].filter(tech => tech.CODE_NAME === 'GP_PROJECT_STATUS');
        }
      },
      (error) =>{
        this.notificationService.danger(error);
      }
    );
  }

  //canDeactivate guard is used to control the exit of the screen when some data is not saved
  canDeactivate():Observable<boolean> | Promise<boolean> | boolean
  {
    if (this.detailsForm.dirty && !this.detailsForm.submitted)
    {
      this.notificationService.warning("Please save changes before exit.");
      return false;
    }
    else 
      return true;
  }

  Back() {
    this._router.navigate(['../../all-projects'], {relativeTo: this._avRoute});
  };

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    if (this.project.PROJECT_REALENDDATE != undefined && this.project.PROJECT_REALSTARTDATE != undefined) {
      if (this.project.PROJECT_REALSTARTDATE > this.project.PROJECT_REALENDDATE) {
        this.notificationService.warning("End date must be greater than Start date");
        if (type == "start") {
          this.project.PROJECT_REALSTARTDATE = null;
        } else {
          this.project.PROJECT_REALENDDATE = null;
        }
      }
    }
   }

  saveProjectDetails() {
    if (this.detailsForm.valid) {
      this.projectService.SaveProjectDetails(this.project).subscribe(
        data => {
          if (data != 0) {
            this.notificationService.success('The project [ ' + this.project.PROJECT_NAME + ' ]' + ' was saved successfully.');
          } else {
            this.notificationService.danger('An error occurred while saving the project.');
          }
        },
        error => {
          this.notificationService.danger(error);
        }
      );
    } else {
      this.notificationService.warning('Please fill all required fields');
    }
  };

  ProjectStartDateChange(event) {
    this.project.PROJECT_REALSTARTDATE = event;
  }

  ProjectEndDateChange(event) {
    this.project.PROJECT_REALENDDATE = event;
  }

}
