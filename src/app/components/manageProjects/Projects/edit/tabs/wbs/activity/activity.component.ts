import { Component, Input, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef} from "@angular/material";
import {transition, trigger, style, animate} from '@angular/animations';

import { DisplayActivityComponent } from '../display/display.component';
import { addEditWBSComponent } from '../addEditWBS/addEditWBS.component';
import { Activity } from 'app/models/manageProject/activity.model';
import { ProjectService, DateTimeService, NotificationService } from 'app/services';
import swal from 'sweetalert2';

@Component({
  selector: 'oc-activity',
  templateUrl: 'activity.component.html',
  styleUrls: ['activity.component.scss']
})
export class ActivityComponent {
  @Input() activity: Activity;
  @Input() parentStartDate: Date;
  @Input() parentEndDate: Date;
  @ViewChild("activityCard") activityCard : ElementRef;

  //isSelected: boolean = false;

  DisplayWbsdialogRef: MatDialogRef<DisplayActivityComponent>;
  WbsdialogRef: MatDialogRef<addEditWBSComponent>;

	constructor(
    private dialog: MatDialog,
    private projectService : ProjectService,
    private dateTimeService : DateTimeService,
    private notificationService: NotificationService
  ) { }

	showChilds(value : boolean) {
    this.activity.showChilds = value;
  }
  
  displayActivity(){
    let config: MatDialogConfig = {
      disableClose: false,
      //panelClass: "display-modal",
      hasBackdrop: true,
      width: '350px',
      height : '330px',
      data: {
        activity: this.activity
       }
    };
    this.showActionBtns();
    this.DisplayWbsdialogRef = this.dialog.open(DisplayActivityComponent, config);
  }

  editActivity(){
    this.openAddEditWbsDialog("edit");
    this.showActionBtns();
  }

  addActivity(){
    this.openAddEditWbsDialog("add");
    this.showActionBtns();
  }

  openAddEditWbsDialog(mode: string) {
    let config: MatDialogConfig = {
      disableClose: true,
      panelClass: "form_modal",
      hasBackdrop: true,
      data: {
        mode: mode,
        activity : this.activity,
        parentStartDate: mode == "edit" ? this.parentStartDate : this.activity.plannedStartDate,
        parentEndDate:  mode == "edit" ? this.parentEndDate : this.activity.plannedEndDate
      }
    };
    this.WbsdialogRef = this.dialog.open(addEditWBSComponent, config);
  }

  deleteActivity(){
    swal({
      //title: 'Are you sure to delete this activity ?',
      text: 'Are you sure to delete this activity ?',
      type: 'error',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      showLoaderOnConfirm: true,
      preConfirm: () => {
        this.projectService.DeleteActivity(this.activity.activityId).subscribe(
          data => {
            this.projectService.ActivityDeleted.next(this.activity);
            this.notificationService.success("The activity '" + this.activity.name + "' is deleted successfully !");
          },
          error => {
            this.notificationService.danger(error);
          }
        );
      }
    });
  }

  getLabelCssClass(value: number): string{
    if (value == 0)
      return "badge badge-default";

    if (value < 1)
      return "badge badge-danger";

    if (value >= 1)
      return "badge badge-success";
  }

  showActionBtns(){
    this.activity.isSelected = !this.activity.isSelected;
    if (this.activity.isSelected)
      //Emit event to deselect other activities
      this.projectService.ActivitySelected.emit(this.activity);
  }
}
