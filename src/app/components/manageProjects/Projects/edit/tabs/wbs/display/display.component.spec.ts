import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayActivityComponent } from './display.component';

describe('DisplayComponent', () => {
  let component: DisplayActivityComponent;
  let fixture: ComponentFixture<DisplayActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
