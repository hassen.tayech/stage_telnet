import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NotificationService } from 'app/services';
import { ProjectService } from 'app/services/project.service';
import { Activity } from 'app/models/manageProject/activity.model';
import { DateTimeService } from 'app/services/dateTime.service';
import { PagerService } from 'app/services/PagerService';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.scss']
})
export class DisplayActivityComponent implements OnInit {

  activity = new Activity();
  activityId: number;
  path: string;
  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];
  constructor(
          private dialogRef: MatDialogRef<DisplayActivityComponent>,
          @Inject(MAT_DIALOG_DATA) private data, private notificationService: NotificationService
        )
  {
      this.activity = this.data.activity;
  }

  ngOnInit() {
    
  };

  getLabelCssClass(value: number): string{
    if (value == 0)
      return "badge badge-default";

    if (value < 1)
      return "badge badge-danger";

    if (value >= 1)
      return "badge badge-success";
  }

}




