import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDatepickerInputEvent } from '@angular/material';
import { NotificationService, ShowErrorsService } from 'app/services';
import { ProjectService } from 'app/services/project.service';
import { Activity, ActivityStatus } from 'app/models/manageProject/activity.model';
import { DateTimeService } from 'app/services/dateTime.service';
import { PagerService } from 'app/services/PagerService';
import { NgForm, FormControl, FormGroup, Validators, FormBuilder, AbstractControl, ValidatorFn } from '@angular/forms';
import { BindModel } from 'app/models/shared';
import { Assignement } from 'app/models/manageProject/assignement.model';
import { CustomValidators } from 'app/shared/custom-validators/custom-validators';
import { max } from 'moment';
//import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

@Component({
  selector: 'addEditWBS_component',
  templateUrl: './addEditWBS.component.html',
  styleUrls: ['./addEditWBS.component.scss']
})

export class addEditWBSComponent implements OnInit {
  public activity = new Activity();
  newAssignement = new Assignement();
  UsersList: BindModel[] = [];
  isEditMode : boolean;
  isReadOnlyMode : boolean;
  assignmentTabLabel : string = "Assignment";
  pager: any = {};
  pagedItems: any[];
  detailsForm: FormGroup;
  public parentStartDate : Date;
  public parentEndDate : Date;
  minStartDate : Date;
  maxStartDate : Date;
  minEndDate : Date;
  maxEndDate : Date;

  constructor(
    private dialogRef: MatDialogRef<addEditWBSComponent>,
    @Inject(MAT_DIALOG_DATA) private dialogData,
    private notificationService: NotificationService,
    private projectService: ProjectService,
    private dtService: DateTimeService,
    private pagerService: PagerService,
    private fb: FormBuilder,
    private showErrorService : ShowErrorsService
  )
  {
      this.isEditMode = this.dialogData.mode == "edit" ? true : false;
      this.parentStartDate = this.dialogData.parentStartDate;
      this.parentEndDate = this.dialogData.parentEndDate;
      this.createForm();
  }

  ngOnInit() {
    if (this.isEditMode) {
      this.activity = this.dialogData.activity;
      if (this.activity.activityId != 0)
        this.getActivityDetails(this.activity.activityId);
    }
    else
    {
      this.insertNewActivityDetails();
    }

    this.rebuildForm();
    this.updateMinMaxDateValues();
    this.getListUsersByProjectId(this.activity.projectId);

    if (this.activity.level == 1)
      this.isReadOnlyMode = true;
    else
      this.isReadOnlyMode = false;
  };

  updateMinMaxDateValues(){
    this.minStartDate = this.parentStartDate;
    this.maxStartDate = this.dtService.getFirstDate(this.parentEndDate, this.detailsForm.controls["plannedEndDate"].value);

    this.minEndDate = this.dtService.getLastDate(this.parentStartDate, this.detailsForm.controls["plannedStartDate"].value);
    this.maxEndDate = this.parentEndDate;
  }

  insertNewActivityDetails(){
    let parentActivity : Activity = this.dialogData.activity;
    if (parentActivity.hasChilds)
      this.activity.activityNumber = parentActivity.activityNumber + "." + (parentActivity.subActivities.length + 1);
    else
      this.activity.activityNumber = parentActivity.activityNumber + ".1";

    this.activity.projectId = parentActivity.projectId;
    this.activity.level = parentActivity.level + 1;
    this.activity.parentId = parentActivity.activityId;
    this.activity.subActivities = [];
    this.activity.assignements = [];
    this.activity.hasChilds = false;
  }

  createForm() {
    this.detailsForm = this.fb.group({
      name:  new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      plannedStartDate: new FormControl('', [Validators.required, startDateValidator(this)]),
      plannedEndDate: new FormControl('', [Validators.required, endDateValidator(this)]),
      estimatedCharge: new FormControl('', [CustomValidators.numeric,Validators.min(0)]),
      plannedDuration: new FormControl('', [CustomValidators.numeric,Validators.min(0)]),
      elapsedDuration: new FormControl('', [CustomValidators.numeric,Validators.min(0)]),
      advancement: new FormControl('', [CustomValidators.numeric, Validators.min(0), Validators.max(100)]),
      actualCost: new FormControl('', [CustomValidators.numeric, Validators.min(0)])
    });
  }

  rebuildForm() {
    this.detailsForm.reset({
      name: this.activity.name,
      description: this.activity.description,
      plannedStartDate: this.activity.plannedStartDate,
      plannedEndDate: this.activity.plannedEndDate,
      estimatedCharge: this.activity.estimatedCharge,
      plannedDuration: this.activity.plannedDuration,
      elapsedDuration: this.activity.elapsedDuration,
      advancement: this.activity.advancement,
      actualCost: this.activity.actualCost
    });
  }

  prepareSaveActivity(): Activity {
    const formModel = this.detailsForm.value;

    const saveActivity: Activity = {
      activityId: this.activity.activityId as number,
      projectId: this.activity.projectId as number,
      parentId: this.activity.parentId as number,
      name: formModel.name as string,
      description: formModel.description as string,
      plannedStartDate: formModel.plannedStartDate as Date,
      plannedEndDate: formModel.plannedEndDate as Date,
      estimatedCharge: formModel.estimatedCharge as number,
      advancement: formModel.advancement as number,
      plannedDuration: formModel.plannedDuration as number,
      actualCost: formModel.actualCost as number,
      elapsedDuration: formModel.elapsedDuration as number
    };
    return saveActivity;
  }

  revert() { 
    this.rebuildForm();
  }

  updateOriginalActivity(savedActivity: Activity){
    //this.activity = Object.assign(Activity, this.activity);
    this.activity.activityId = savedActivity.activityId;
    this.activity.name = savedActivity.name;
    this.activity.description = savedActivity.description;
    this.activity.plannedStartDate = savedActivity.plannedStartDate;
    this.activity.plannedEndDate = savedActivity.plannedEndDate;
    this.activity.estimatedCharge = savedActivity.estimatedCharge;
    this.activity.advancement = savedActivity.advancement;
    this.activity.plannedDuration = savedActivity.plannedDuration;
    this.activity.actualCost = savedActivity.actualCost;
    this.activity.realEndDate = savedActivity.realEndDate;
    this.activity.status = savedActivity.status;
    this.activity.earnedValue = savedActivity.earnedValue;
    this.activity.spi = savedActivity.spi;
    this.activity.cpi = savedActivity.cpi;
    this.activity.elapsedDuration = savedActivity.elapsedDuration;
  }

  getActivityDetails(activityId: number) {
    this.projectService.GetActivityDetails(activityId).subscribe(
      data => {
        this.activity.assignements = data.assignements;
        if (this.activity.assignements.length > 0)
          this.assignmentTabLabel = "Assignment (" + this.activity.assignements.length + ")";

        this.setPage(1);
      },
      error => {
        this.notificationService.danger(error);
      }
    );
  };

  saveActivityDetails() {
    const savedActivity = this.prepareSaveActivity();

    if (this.activity.advancement != savedActivity.advancement){
      if (savedActivity.advancement == 100){
        savedActivity.realEndDate = this.dtService.today();
        savedActivity.status = ActivityStatus.Completed;
      }
      else{
        savedActivity.status = ActivityStatus.Started;
        savedActivity.realEndDate = null;
      }
    }
    else{
      savedActivity.status = this.activity.status;
      savedActivity.realEndDate = this.activity.realEndDate;
    }

    this.projectService.saveActivity(savedActivity).subscribe(
      id => {
        if (!this.isEditMode)
        {
          savedActivity.activityId = id;
          this.projectService.ActivityAdded.next(this.activity);
          this.isEditMode = true;
        }

        this.updateOriginalActivity(savedActivity);

        if (  (this.detailsForm.controls["estimatedCharge"].dirty)
           || (this.detailsForm.controls["advancement"].dirty))
          this.projectService.ActivityDetailsChanged.emit();

        //this.updateElapsedDuration();
        this.updateAllKpis();
        this.rebuildForm();
        this.notificationService.success("The WBS " + this.activity.name + " is saved successfully.");
      },
      error => {
        this.notificationService.danger(error);
      }
    );
  }

  updateEstimatedCharge(){
    var estimatedCharge = 0;
    this.activity.assignements
      .forEach( ass => { estimatedCharge += +ass.estimatedCharge; } );
    
    this.detailsForm.patchValue({
      estimatedCharge: estimatedCharge
    });
    this.detailsForm.controls["estimatedCharge"].markAsDirty();
  }

  AddAssignement(form: NgForm) {
    this.newAssignement.activityId = this.activity.activityId;
    this.newAssignement.projectId = this.activity.projectId;

    this.projectService.AddAssignements(this.newAssignement).subscribe(
      data => {
        this.activity.assignements.push(...data);
        this.assignmentTabLabel = "Assignment (" + this.activity.assignements.length + ")";
        this.setPage(1);
      },
      error => {
        this.notificationService.danger(error);
      }
    );
  };
 

  close() {
    this.dialogRef.close();
  };

  getListUsersByProjectId(projectId: number) {
    this.projectService.GetUsersByProjectId(projectId).subscribe(
      data => {
        this.UsersList = data;
      },
      error => {
        this.notificationService.danger(error);
      }
    );
  }

  setPage(page: number) {
    if (page < 1) {
      return;
    }
    // get pager object from service
    this.pager = this.pagerService.getPager(this.activity.assignements.length, page);
    // get current page of items
    this.pagedItems = this.activity.assignements.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  activityDateChange(type: string, event: MatDatepickerInputEvent<Date>) {
      if (this.detailsForm.controls["plannedStartDate"].valid && this.detailsForm.controls["plannedEndDate"].valid){
        this.updatePlannedDuration();
        this.updateMinMaxDateValues();
        this.updateElapsedDuration();
      }
  }

  assignmentDateChange(type: string, event: MatDatepickerInputEvent<Date>) {
    if (this.newAssignement.userPlannedStartDate != undefined && this.newAssignement.userPlannedEndDate != undefined) {
      if (!this.dtService.checkFirstAndLastDateOrder(this.newAssignement.userPlannedStartDate, this.newAssignement.userPlannedEndDate)){
        if (type == "start") {
          this.notificationService.warning("Start Date must be lower than End Date");
          this.newAssignement.userPlannedStartDate = null;
        } else {
          this.notificationService.warning("End Date must be greater than Start Date");
          this.newAssignement.userPlannedEndDate = null;
        }
      }
    }
  }

  updatePlannedDuration(){
    this.detailsForm.patchValue({
      plannedDuration: this.dtService.calculateBusinessDays(this.detailsForm.controls["plannedStartDate"].value,
                                                            this.detailsForm.controls["plannedEndDate"].value)
    });

    this.detailsForm.controls["plannedDuration"].markAsDirty();
  }
  updateElapsedDuration(){
    switch(this.activity.status){
      case ActivityStatus.NotStarted:
        this.detailsForm.patchValue({
          elapsedDuration : 0
        });
        break;

      case ActivityStatus.Started:
        this.detailsForm.patchValue({
          elapsedDuration : this.dtService.calculateBusinessDays(this.detailsForm.controls["plannedStartDate"].value, this.dtService.today(), false)
        });
        break;

      case ActivityStatus.Completed:
        if (!this.activity.hasChilds){
          this.detailsForm.patchValue({
            elapsedDuration : this.dtService.calculateBusinessDays(this.detailsForm.controls["plannedStartDate"].value, this.activity.realEndDate)
          });
        }
        else{
          var realEndDate : Date = null;
          this.activity.subActivities.forEach(item => {
            realEndDate = this.dtService.getLastDate(realEndDate, item.realEndDate);
          })
          this.detailsForm.patchValue({
            elapsedDuration : this.dtService.calculateBusinessDays(this.detailsForm.controls["plannedStartDate"].value, realEndDate)
          });
        }
        break;
    }
  }

  updateElapsedDuration2(){
    switch(this.activity.status){
      case ActivityStatus.NotStarted:
        this.activity.elapsedDuration = 0;
        break;

      case ActivityStatus.Started:
        this.activity.elapsedDuration = this.dtService.calculateBusinessDays(this.activity.plannedStartDate, this.dtService.today(), false);
        break;

      case ActivityStatus.Completed:
        if (!this.activity.hasChilds){
          this.activity.elapsedDuration = this.dtService.calculateBusinessDays(this.activity.plannedStartDate, this.activity.realEndDate);
        }
        else{
          var realEndDate : Date = null;
          this.activity.subActivities.forEach(item => {
            realEndDate = this.dtService.getLastDate(realEndDate, item.realEndDate);
          })
          this.activity.elapsedDuration = this.dtService.calculateBusinessDays(this.activity.plannedStartDate, realEndDate);
        }
        break;
    }
  }

  updateUserEstimCharge(){
    this.newAssignement.estimatedCharge = 
      this.dtService.calculateBusinessDays(this.newAssignement.userPlannedStartDate, this.newAssignement.userPlannedEndDate);
  }

  updateAllKpis(){
    this.updateEarnedValue();
    this.updateSpi();
    this.updateCpi();
  }

  updateEarnedValue(){
    this.activity.earnedValue = +((this.activity.advancement / 100) * this.activity.estimatedCharge).toFixed(2);
  }

  updateSpi(){
    if (this.activity.elapsedDuration != 0)
      this.activity.spi = +(((this.activity.advancement / 100) * this.activity.plannedDuration) / this.activity.elapsedDuration).toFixed(2);
    else
      this.activity.spi=0;
  }

  updateCpi(){
    if (this.activity.actualCost != 0)
      this.activity.cpi = +(this.activity.earnedValue/this.activity.actualCost).toFixed(2);
    else
      this.activity.cpi=0;
  }

  isSaveBtnDisabled(){
    return this.isReadOnlyMode
           || this.detailsForm.invalid 
           || this.detailsForm.pristine 
           || this.detailsForm.untouched;
  }

  isCancelBtnDisabled(){
    return this.isReadOnlyMode
           || this.detailsForm.pristine 
           || this.detailsForm.untouched;
  }

  isCloseBtnDisabled(){
    return  this.detailsForm.dirty 
           || this.detailsForm.touched;
  }
}


function startDateValidator(that: any): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    let startDate : Date = control.value as Date;
    let activity = that.activity;
    
    if (that.dtService.checkFirstAndLastDateOrder(startDate, that.parentStartDate))
      return { 'parentStartDate': true };
    
    if (!that.dtService.checkFirstAndLastDateOrder(startDate, activity.plannedEndDate))
      return { 'plannedEndDate': true };

    if (activity.hasChilds){
      if (activity.subActivities.some(item => that.dtService.checkFirstAndLastDateOrder(item.plannedStartDate, startDate)))
        return { 'childStartDate': true };
    }
    return null; 
  };
}

function endDateValidator(that: any): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    let endDate : Date = control.value as Date;
    let activity = that.activity;
    
    if (that.dtService.checkFirstAndLastDateOrder(that.parentEndDate, endDate))
      return { 'parentEndDate': true };
    
    if (!that.dtService.checkFirstAndLastDateOrder(activity.plannedStartDate, endDate))
      return { 'plannedStartDate': true };

    if (activity.hasChilds){
      if (activity.subActivities.some(
          item => {
            return !that.dtService.checkFirstAndLastDateOrder(item.plannedEndDate, endDate) && !that.dtService.areEqualDates(item.plannedEndDate, endDate);
          }))
        return { 'childEndDate': true };
    }
    return null; 
  };
}
