import { Component, OnInit, Input, ViewChild, OnDestroy, HostListener, ElementRef } from '@angular/core';
import { forEach } from '@angular/router/src/utils/collection';
import { Subscription } from 'rxjs/Subscription';
import swal from 'sweetalert2';

import { ProjectService, NotificationService, DateTimeService, WbsDisplayHandlerService } from 'app/services';
import { addEditWBSComponent } from './addEditWBS/addEditWBS.component';
import { Activity, ActivityStatus } from 'app/models/manageProject/activity.model';
import { ProjectModel } from 'app/models/manageProject';


@Component({
  selector: 'wbs-component',
  templateUrl: 'wbs.component.html',
  styleUrls: ['wbs.component.scss']
})
export class wbsComponent implements OnInit, OnDestroy {
  topActivity: Activity;
  selectedActivity: Activity = new Activity();
  deleteActivitySubscrip: Subscription;
  addActivitySubscrip: Subscription;
  
  displayChildsOffset : number = 400;

  @Input() project: ProjectModel;
  @ViewChild('wbsContainer') wbsContainer: ElementRef;

  constructor(
    private projectService: ProjectService,
    private notificationService: NotificationService,
    private dtService: DateTimeService,
    private wbsDisplayHandler: WbsDisplayHandlerService)
  { }

  
  ngOnInit() {
   
    this.GetActivitiesById(this.project.PROJECT_ID);

    this.projectService.ActivityDetailsChanged
      .subscribe(
        () => {
          this.calculateWbsEstimCharge(this.topActivity);
          this.calculateWbsAdvancement(this.topActivity);
          this.calculateWbsAcwpBcwpKpis(this.topActivity);
        }
      );

    this.projectService.ActivitySelected
      .subscribe(
        (selectedActivity : Activity) => {
          this.deselectActivity(this.topActivity, selectedActivity.activityId);
        }
      )
    
    this.addActivitySubscrip = 
      this.projectService.ActivityAdded
        .subscribe(
          (newActivity : Activity) => {
            this.addActivity(newActivity);
          }
        );

    this.deleteActivitySubscrip =
      this.projectService.ActivityDeleted
        .subscribe(
          (activityToDelete : Activity) => {
            this.deleteActivity(activityToDelete);
          }
        )
  }
  
  ngOnDestroy(){
    this.deleteActivitySubscrip.unsubscribe();
    this.addActivitySubscrip.unsubscribe();
  }
  
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.wbsDisplayHandler.zoomAuto(window.innerWidth);
  }

  addActivity(newActivity: Activity){
    var parentActivity = this.getParentActivity(newActivity.activityNumber);
    parentActivity.subActivities.push(newActivity);
    parentActivity.showChilds = true;
    parentActivity.hasChilds = true;
    newActivity.hasParent = true;
    this.calculateWbsAutoValues();
    this.wbsDisplayHandler.zoomAuto();
  }

  deleteActivity(activityToDelete: Activity){
    var parentActivity = this.getParentActivity(activityToDelete.activityNumber);
    
    //update parent activity and other sub activities data
    parentActivity.subActivities = parentActivity.subActivities.filter(ac => ac.activityId != activityToDelete.activityId);
    if (parentActivity.subActivities.length > 0)
      this.updateSubActivitiesNumber(parentActivity);
    else{
      parentActivity.estimatedCharge = 0;
      parentActivity.advancement = 0;
      parentActivity.hasChilds = false;
    }

    // Update wbs auto values : estimated charge, advancement,...
    this.calculateWbsAutoValues();
    //this.wbsDisplayHandler.zoomAuto();
  }

  GetActivitiesById(id: string) {
    this.projectService.GetActivitiesByProjectId(id).subscribe(
      data => {
        this.initializeTopActivity(data);
        this.calculateWbsAutoValues();
        this.wbsDisplayHandler.init(this.wbsContainer, this.topActivity);
        this.wbsDisplayHandler.zoomAuto();
      },
      error => {
        this.notificationService.danger(error);
      }
    );
  }

  initializeTopActivity(data: any){
    this.topActivity = data;
    this.topActivity.subActivities = data.subActivities == null ? [] : data.subActivities;
    this.topActivity.projectId = +this.project.PROJECT_ID;
    this.topActivity.name = this.project.PROJECT_NAME;
    this.topActivity.plannedStartDate = this.project.PROJECT_BASELINESTARTDATE;
    this.topActivity.plannedEndDate = this.project.PROJECT_BASELINEENDDATE;

    this.topActivity.plannedDuration =  
      this.dtService.calculateBusinessDays(this.topActivity.plannedStartDate,
                                                 this.topActivity.plannedEndDate);
  }

  getParentActivity(childActivityNumber : string): Activity{
    let indexs : string [] = childActivityNumber.split(".");
    let parentActivity : Activity = this.topActivity;
    if (indexs.length > 2)
    {
      for (let i=1; i < indexs.length-1; i++ )
      {
        // "+" is used to convert indexs[i] to number
         parentActivity = parentActivity.subActivities[+indexs[i]-1];
      }
    }
    return parentActivity;
  }

  updateSubActivitiesNumber(parentActivity : Activity){
    for (let i=0; i < parentActivity.subActivities.length; i++ )
    {
      parentActivity.subActivities[i].activityNumber = parentActivity.activityNumber + "." + ( i + 1).toString();
      if (parentActivity.subActivities[i].subActivities.length > 0)
        this.updateSubActivitiesNumber(parentActivity.subActivities[i]);
    }
  }

  calculateWbsAutoValues(){
    this.calculateWbsEstimCharge(this.topActivity);
    this.calculateWbsAdvancement(this.topActivity);
    this.calculateWbsStatus(this.topActivity);
    this.calculateWbsAcwpBcwpKpis(this.topActivity);
    this.calculateElapsedDuration(this.topActivity);
    this.calculateWbsSpiCpiKpis(this.topActivity);
    this.calculateWbsColumnNumber(this.topActivity);
    this.calculateWbsColumnNumberDisplayed(this.topActivity);
    this.calculateWbsMaxLevel(this.topActivity);
    this.calculateWbsMaxLevelDisplayed(this.topActivity);
  }

  calculateWbsEstimCharge(act: Activity){
    if (act.hasChilds){
      act.estimatedCharge = 0;
      act.subActivities.forEach(item => {
        this.calculateWbsEstimCharge(item);
        act.estimatedCharge += +item.estimatedCharge;  
      })
    } 
  }

  calculateWbsAdvancement(act: Activity){
    if (act.hasChilds && act.estimatedCharge > 0){
      act.advancement = 0;
      act.subActivities.forEach(item => {
        this.calculateWbsAdvancement(item);
        act.advancement += (item.estimatedCharge / act.estimatedCharge) * item.advancement;  
      })
      act.advancement = +(act.advancement.toFixed());
    }
  }

  calculateWbsStatus(act: Activity){
    if (!this.dtService.checkFirstAndLastDateOrder(act.plannedStartDate, this.dtService.today()))
      act.status = ActivityStatus.NotStarted;
    else if (act.advancement == 100)
      act.status = ActivityStatus.Completed;
    else
      act.status = ActivityStatus.Started;

    if (act.hasChilds){
      act.subActivities.forEach(item => {
        this.calculateWbsStatus(item);
      })
    } 
  }

  calculateWbsAcwpBcwpKpis(act: Activity){
    if (act.hasChilds){
      act.actualCost = 0;
      act.earnedValue = 0;
      act.subActivities.forEach(item => {
        this.calculateWbsAcwpBcwpKpis(item);
        act.actualCost += item.actualCost;
        act.earnedValue += item.earnedValue;
      })
    }
    else{
      act.earnedValue = +((act.advancement / 100) * act.estimatedCharge).toFixed(2);
    }
  }

  calculateWbsSpiCpiKpis(act: Activity){

    //spi
    if (act.elapsedDuration > 0)
      act.spi = +(((act.advancement / 100) * act.plannedDuration) / act.elapsedDuration).toFixed(2);
    else
      act.spi = 0;
    
    //cpi
    if (act.actualCost > 0)
      act.cpi = +(act.earnedValue / act.actualCost).toFixed(2);
    else
      act.cpi = 0;
    
    if (act.hasChilds){
      act.subActivities.forEach(item => {
        this.calculateWbsSpiCpiKpis(item);
      })
    }
    
  }

  // calculateElapsedDuration(act: Activity){
  //   if (act.advancement < 100)
  //     act.elapsedDuration = this.dtService.calculateBusinessDays(act.plannedStartDate, this.dtService.today(), false);
  //   else if (!act.hasChilds){
  //     act.elapsedDuration = this.dtService.calculateBusinessDays(act.plannedStartDate, act.realEndDate);
  //   }
  //   else{
  //     var realEndDate : Date = null;
  //     act.subActivities.forEach(item => {
  //       realEndDate = this.dtService.getLastDate(realEndDate, item.realEndDate);
  //     })
  //     act.elapsedDuration = this.dtService.calculateBusinessDays(act.plannedStartDate, realEndDate);
  //   }
  // }

  calculateElapsedDuration(act: Activity){
    switch(act.status){
      case ActivityStatus.NotStarted:
        act.elapsedDuration = 0;
        break;

      case ActivityStatus.Started:
        act.elapsedDuration = this.dtService.calculateBusinessDays(act.plannedStartDate, this.dtService.today(), false);
        break;

      case ActivityStatus.Completed:
        if (!act.hasChilds){
          act.elapsedDuration = this.dtService.calculateBusinessDays(act.plannedStartDate, act.realEndDate);
        }
        else{
          var realEndDate : Date = null;
          act.subActivities.forEach(item => {
            this.calculateElapsedDuration(item);
            if (item.advancement == 100)
              realEndDate = this.dtService.getLastDate(realEndDate, item.realEndDate);
          })
          act.elapsedDuration = this.dtService.calculateBusinessDays(act.plannedStartDate, realEndDate);
          act.realEndDate = realEndDate;
        }
        break;
    }

    if (act.hasChilds){
      act.subActivities.forEach(item =>{
        this.calculateElapsedDuration(item);
      });
    }
  }


  // calculateWbsKpiAcwp(act: Activity){
  //   if (act.hasChilds){
  //     act.actualCost = 0;
  //     act.subActivities.forEach(item => {
  //       this.calculateWbsKpiAcwp(item);
  //       act.actualCost += item.actualCost;  
  //     })
  //   }
  // }

  // calculateWbsKpiBcwp(act: Activity){
  //   if (act.hasChilds){
  //     act.earnedValue = 0;
  //     act.subActivities.forEach(item => {
  //       this.calculateWbsKpiBcwp(item);
  //       act.earnedValue += item.earnedValue;  
  //     })
  //   }
  //   else{
  //     act.earnedValue = ((act.advancement * act.estimatedCharge) / 100);
  //   }
  // }

  calculateWbsColumnNumber(act: Activity){
    if (act.hasChilds) {
      act.columnNumber = 0;
      act.subActivities.forEach(item => {
        this.calculateWbsColumnNumber(item);
        act.columnNumber += item.columnNumber; 
      })
    } 
    else {
      act.columnNumber = 1;
    }
  }

  calculateWbsColumnNumberDisplayed(act: Activity){
    if (act.hasChilds && act.showChilds) {
      act.columnNumberDisplayed = 0;
      act.subActivities.forEach(item => {
        this.calculateWbsColumnNumberDisplayed(item);
        act.columnNumberDisplayed += item.columnNumberDisplayed; 
      })
    } 
    else {
      act.columnNumberDisplayed = 1;
    }
  }

  calculateWbsMaxLevel(act: Activity){
    if (act.hasChilds) {
      act.maxLevel = 0;
      act.subActivities.forEach(item => {
        this.calculateWbsMaxLevel(item);
        act.maxLevel = Math.max(act.maxLevel, +item.level);  
      })
    } 
    else {
      act.maxLevel = +act.level;
    }
  }

  calculateWbsMaxLevelDisplayed(act: Activity){
    if (act.hasChilds && act.showChilds) {
      act.maxLevelDisplayed = 0;
      act.subActivities.forEach(item => { 
        this.calculateWbsMaxLevelDisplayed(item);
        act.maxLevelDisplayed = Math.max(act.maxLevelDisplayed, item.maxLevelDisplayed);  
      })
    } 
    else {
      act.maxLevelDisplayed = +act.level;
    }
  }

  showAllChilds() {
    if (this.topActivity.hasChilds){
      this.showChilds(this.topActivity, 0);
      this.topActivity.columnNumberDisplayed = this.topActivity.columnNumber;
    }
  }

  showChilds(act: Activity, timeOut: number) {
    setTimeout(() => {
      act.showChilds = true;
    }, timeOut);

    if (act.hasChilds) {
      var newTimeOut = timeOut + this.displayChildsOffset;
      act.subActivities.forEach(item => {
          this.showChilds(item, newTimeOut);
      })
    }
  }
 
  hideAllChilds() {
    if (this.topActivity.hasChilds){
      this.calculateWbsMaxLevelDisplayed(this.topActivity);
      var timeOut = this.displayChildsOffset * (this.topActivity.maxLevelDisplayed - 1);
      this.hideChilds(this.topActivity, timeOut);
    }  
  }

  hideChilds(act: Activity, timeOut: number){
    setTimeout(() => {
      act.showChilds = false;
    }, timeOut);

    if (act.hasChilds) {
      var newTimeOut = timeOut - this.displayChildsOffset;
      act.subActivities.forEach(item => {
          this.hideChilds(item, newTimeOut);
      })
    }
  }

  resetWbsDisplay(){
    this.calculateWbsColumnNumberDisplayed(this.topActivity);
    this.wbsDisplayHandler.resetWbsDisplay();
  }

  deselectActivity(act: Activity, exceptedActivityId: number){
    act.isSelected = (act.activityId == exceptedActivityId);
    if (act.hasChilds) {
      act.subActivities.forEach(item => {
          this.deselectActivity(item, exceptedActivityId);
      })
    }
  }
}
