import { Component, Input } from '@angular/core';
import { Activity } from 'app/models/manageProject/activity.model';
import {expandCollapse} from 'app/animations';


@Component({
	selector: 'ng-org-chart',
	templateUrl: './org-chart.component.html',
	styleUrls: ['./org-chart.component.css'],
	animations: [expandCollapse]
})
export class OrgChartComponent {
  @Input() topActivity: Activity;
  @Input() topActivityStartDate: Date;
  @Input() topActivityEndDate: Date;
}
