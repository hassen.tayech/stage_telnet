import { Directive, OnInit, Input, HostBinding } from '@angular/core';


@Directive({
    selector: '[wbs-activity-Color]'
})

export class WbsActivityColorDirective implements OnInit {
    @Input() defaultColor: string = 'transparent';
    @Input('wbs-activity-Color') levelNumber: number;
    @HostBinding('style.backgroundColor') backgroungColor: string;
    @HostBinding('style.borderColor') borderColor: string;

    ngOnInit() {
       switch(this.levelNumber){
        case 1:
          this.backgroungColor = "#FFF7C2";//"#EC4A89";
          this.borderColor = "#FECB16";
        break;

        case 2:
          this.backgroungColor = "#CCECFF";//"#448aff";
          this.borderColor = "#5AB6FF";
        break;

        case 3:
          this.backgroungColor = "#D2ECD2";//"#fd7e14";
          this.borderColor = "#2B920E";
        break;

        case 4:
          this.backgroungColor = "#E5D4EB";//"#dbdb4a";
          this.borderColor = "#A02A95";
        break;

        case 5:
          this.backgroungColor = "#FFD9DA";//"#70c24a";
          this.borderColor = "#A62B39";
        break;

        default:
          this.backgroungColor = this.defaultColor;
          this.borderColor = "#0000";
       }
    }
    
}
