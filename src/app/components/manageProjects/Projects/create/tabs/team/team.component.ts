import { Component, OnInit, Input, OnChanges, SimpleChanges, SimpleChange, Output, EventEmitter, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { ProjectService, NotificationService } from 'app/services';
import { Member } from 'app/models/manageProject/member.model';
import { Observable } from 'rxjs/Observable';
import { PagerService } from '../../../../../../services/PagerService';
import { DateTimeService } from '../../../../../../services/dateTime.service';
import { NgForm } from '@angular/forms';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-team',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit, OnChanges {
  @Input() PifId: number;
  @Input() ActivityId: string;
  @Input() ProjectStartDate: Date;
  @Input() ProjectEndDate: Date;
  @Input() disableEdit:boolean;

  @Output('handleProjectStartDateChange') ProjectStartDateChange = new EventEmitter<Date>();

  @Output('handleProjectEndDateChange') ProjectEndDateChange = new EventEmitter<Date>();

  itemList = [];
  selectedItems = [];
  MembersList = [];
  Roles = [];
  selectedRole = this.Roles[0];
  member: Member;
  tabList = [];
  StartDate: Date;
  EndDate: Date;
  Reponsability: string;
  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];


  constructor(private projectService: ProjectService,
    private notificationService: NotificationService,
    private pagerService: PagerService,
    private dateTimeService: DateTimeService,
    private cd: ChangeDetectorRef) { }

  ngOnInit() {
    this.GetRoles();
    this.GetMemberListByActivity(this.ActivityId);
    if (this.PifId != 0 && this.PifId != undefined)
      this.GetTeamByPifId(this.PifId)
  }

  ngOnChanges(changes: SimpleChanges) {
    const currentActivityId: SimpleChange = changes.ActivityId;
    const currentProjectStartDate: SimpleChange = changes.ProjectStartDate;
    const currentProjectEndDate: SimpleChange = changes.ProjectEndDate;
    if (currentActivityId != undefined) {
      this.GetMemberListByActivity(currentActivityId.currentValue);
    } else if (currentProjectStartDate != undefined) {
      var members = this.tabList.find(x => new Date(x.startDate) < new Date(currentProjectStartDate.currentValue) || new Date(x.startDate) > this.ProjectEndDate);
      if (members != null) {
        this.ProjectStartDateChange.emit(currentProjectStartDate.previousValue);
        this.notificationService.warning("You have (a) team member(s) scheduled to start in date less/greater than planned start/end project date ");
        this.cd.detectChanges();
      }
    } else if (currentProjectEndDate != undefined) {
      var members = this.tabList.find(x => new Date(x.EndDate) < this.ProjectStartDate || new Date(x.EndDate) > new Date(currentProjectEndDate.currentValue));
      if (members != null) {
        this.ProjectEndDateChange.emit(currentProjectEndDate.previousValue);
        this.notificationService.warning("You have (a) team member(s) scheduled to finish in date less/greater than planned start/end project date");
        this.cd.detectChanges();
      }
    }


  }

  

  setPage(page: number) {
    if (page < 1) {
      return;
    }

    // get pager object from service
    this.pager = this.pagerService.getPager(this.tabList.length, page);

    // get current page of items
    this.pagedItems = this.tabList.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  GetRoles() {
    this.projectService.GetAllRoles()
      .subscribe(data => {
        for (let i = 0; i < data.length; i++) {
          this.Roles.push({ "id": data[i].ROLE_ID, "name": data[i].ROLE_NAME })
        }

      },
      error => {
        this.notificationService.danger(error);
      }

      );
  }

  GetTeamByPifId(pifId) {
    this.projectService.GetPifTeam(pifId)
      .subscribe(data => {
        for (var i = 0; i < data.length; i++) {
          this.tabList.push({ "name": data[i].Member_NAME, "memberId": data[i].ID_USER, "role": data[i].ROLE, "roleId": data[i].ROLEID, "startDate": this.dateTimeService.GetDate(data[i].START_DATE), "EndDate": this.dateTimeService.GetDate(data[i].END_DATE) });
        }
        this.setPage(1);
      },
      error => {
        this.notificationService.danger(error);
      }

      );
  }

  AddMember(form: NgForm) {
    if (form.valid) {
      var elem: Member = new Member(this.PifId, this.selectedRole.id);
      elem.IdUsers = this.selectedItems.map(({ id }) => id);
      elem.START_DATE = this.StartDate;
      elem.END_DATE = this.EndDate;
      this.projectService.AddMember(elem)
        .subscribe(data => {
          for (var i = 0; i < this.selectedItems.length; i++) {
            var IsExist = this.tabList.findIndex(x => x.memberId == this.selectedItems[i].id && x.roleId == this.selectedRole.id) > -1;
            if (!IsExist) {
              var startDate = this.dateTimeService.FormatDateToString(this.StartDate);
              var endDate = this.dateTimeService.FormatDateToString(this.EndDate);
              this.tabList.unshift({ "name": this.selectedItems[i].itemName, "memberId": this.selectedItems[i].id, "role": this.selectedRole.name, "roleId": this.selectedRole.id, "startDate": startDate, "EndDate": endDate });
            }

          }
          this.setPage(1);
          form.resetForm();
         
        },
        error => {
          this.notificationService.danger(error);
        });
    } else {
      this.notificationService.warning('Please fill all required fields');
    }
  }

  RemoveMember(roleId, memberId) {
    var memberToDelete = new Member(this.PifId, roleId);
    memberToDelete.ID_USER = memberId;

    this.projectService.RemoveMember(memberToDelete).subscribe(
      data => {
        var indexMemberToDelete = this.tabList.findIndex(x => x.memberId == memberId && x.roleId == roleId);
        if (indexMemberToDelete > -1) {
          this.tabList.splice(indexMemberToDelete, 1);
          this.setPage(1);
        }
      },
      error => {
        this.notificationService.danger(error);
      }
    );
  }

  addEvent(type: string) {

    if (type == "start") {
      if (this.StartDate.valueOf() < this.dateTimeService.ConvertDateToNumber(this.ProjectStartDate) || this.StartDate.valueOf() > this.dateTimeService.ConvertDateToNumber(this.ProjectEndDate)) {
        this.notificationService.warning("Start date must between or equal to " + this.dateTimeService.getShortFormat(this.ProjectStartDate) + " - " + this.dateTimeService.getShortFormat(this.ProjectEndDate));
        this.StartDate = null;
      }
    } else {
      if (this.EndDate.valueOf() > this.dateTimeService.ConvertDateToNumber(this.ProjectEndDate) || this.EndDate.valueOf() < this.dateTimeService.ConvertDateToNumber(this.ProjectStartDate)) {
        this.notificationService.warning("End date must between or equal to " + this.dateTimeService.getShortFormat(this.ProjectStartDate) + " - " + this.dateTimeService.getShortFormat(this.ProjectEndDate));
        this.EndDate = null;
      }
    }

    if (this.StartDate != undefined && this.EndDate != undefined) {
      if (this.StartDate > this.EndDate) {
        this.notificationService.warning("End date must greater than Start date");
        if (type == "start") {
          this.StartDate = null;
        } else {
          this.EndDate = null;
        }
      }
    }

  }


  GetMemberListByActivity(activityId: string) {
    this.itemList = [];
    this.projectService.GetMemberListByActivity(activityId)
      .subscribe(data => {
        for (var i = 0; i < data.length; i++) {
          this.itemList.push({ "id": data[i].USER_ID, "itemName": data[i].USER_FIRSTNAME + ' ' + data[i].USER_LASTNAME })
        }
      });

  }
}
