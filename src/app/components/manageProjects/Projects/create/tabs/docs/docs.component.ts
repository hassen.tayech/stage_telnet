import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { ProjectService, NotificationService } from 'app/services';
import { BindModel } from 'app/models/shared';
import { Doc } from 'app/models/manageProject/document.model';
import { Http } from '@angular/http';
import { PagerService } from '../../../../../../services/PagerService';
import * as _ from 'underscore';
import { DateTimeService } from '../../../../../../services/dateTime.service';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-docs',
  templateUrl: './docs.component.html',
  styleUrls: ['./docs.component.scss']
})
export class DocsComponent implements OnInit {
  @Input() PifId: number;
  @Input() disableEdit:boolean;
  public typeEntriesList: BindModel[];
  public selectedTypeEntrie: BindModel;
  public docList: Doc[] = new Array();
  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];

  model = new Doc();
  uploader: FileUploader = new FileUploader({
    url: 'http://localhost:63958/api/uploads/',
    headers: [{ name: 'Accept', value: 'application/json' }]
  });
  @ViewChild('doc') selectedDoc: any;


  constructor(private projectService: ProjectService,
    private notificationService: NotificationService, private pagerService: PagerService,
    private dateTimeService: DateTimeService) {

  }

  ngOnInit() {
    this.GetTypeEntrieList();
    if (this.PifId != 0 && this.PifId != undefined)
      this.GetListDoc(this.PifId)
    //this.GetListDoc(69);
    //override the onAfterAddingfile property of the uploader so it doesn't authenticate with //credentials.
    this.uploader.onAfterAddingFile = (newFile) => {
      this.model.fileName = newFile.file.name;
      newFile.withCredentials = false;
    };
    //overide the onCompleteItem property of the uploader so we are 
    //able to deal with the server response.
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
    };
  };

  setPage(page: number) {

    //if (page < 1 || page > this.pager.totalPages) {
    //  return;
    //}

    if (page < 1 ) {
      return;
    }
    // get pager object from service
    this.pager = this.pagerService.getPager(this.docList.length, page);
    // get current page of items
    this.pagedItems = this.docList.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  GetTypeEntrieList() {
    this.projectService.GetTypeEntriesList().subscribe(
      data => {
        this.typeEntriesList = data;
      },
      error => {
        this.notificationService.danger(error);
      }
    );
  };


  add(form: NgForm) {
    this.model.fileName = this.uploader.queue.reverse()[0].file.name;
    this.model.IdPif = this.PifId;
    this.model.typeId = this.selectedTypeEntrie.Id;
    this.projectService.saveDoc(this.model).subscribe(
      data => {
        this.model.id = data;

        var fileToUpload = this.uploader.queue.reverse()[0].file;
        var items = fileToUpload.name.split(".");
        var extension = items[items.length - 1];
        fileToUpload.name = data + "." + extension;

        //call web service to post file
        this.uploader.queue.reverse()[0].upload();
        this.model.editionDate = new Date();
        this.model.typeDesc = this.selectedTypeEntrie.Description;
        this.model.fileExtension = extension;
        this.model.filePath = "http://localhost:63958/api/DownloadFile?fileId=" + data + "&fileName=" + this.model.fileName;
        this.docList.unshift(this.model);
        this.setPage(1);
        this.selectedTypeEntrie = new BindModel();
        //form.resetForm();
        this.model = new Doc();
        this.selectedDoc.nativeElement.value = "";

      },
      error => {
        this.notificationService.danger(error);
      }
    );
  }

  GetListDoc(pifId: number) {
    this.projectService.GetListDoc(pifId).subscribe(
      data => {
        this.docList = data;
        this.setPage(1);
      },
      error => {
        this.notificationService.danger(error);
      }
    );
  }

  RemoveEntry(id, fileExtension) {
    var docToDelete = new Doc();
    docToDelete.id = id,
      docToDelete.fileExtension = fileExtension;
    this.projectService.RemoveEntry(docToDelete).subscribe(
      data => {

        //remove from
        var indexDocToDelete = this.docList.findIndex(x => x.id == id);
        if (indexDocToDelete > -1) {
          this.docList.splice(indexDocToDelete, 1);
          this.setPage(1);
        }
      },
      error => {
        this.notificationService.danger(error);
      }
    );
  }


  IsUploaded() {
    return this.uploader.queue != null && this.uploader.queue.length > 0
  }




}
