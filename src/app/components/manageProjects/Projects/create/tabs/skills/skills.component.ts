import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
//import { DateAdapter, MatDatepickerInputEvent, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material';
import * as moment from 'moment';

import { ProjectService, NotificationService, DateTimeService, PagerService } from 'app/services';
import { RequiredSkill } from 'app/models/manageProject';
import { BindModel } from 'app/models/shared';
import { User } from 'app/models/user';
import { NgForm, FormGroup } from '@angular/forms';
import { Form } from '@angular/forms/src/directives/form_interface';
import { NotificationsService } from 'angular2-notifications';
import { MatDatepickerInputEvent } from '@angular/material';


declare const $: any;

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {
  @Input() PifId: number;
  @Input() ProjectStartDate:Date;
  @Input() ProjectEndDate:Date;
  @Input() disableEdit:boolean;

  selectedExperienceLevel: BindModel;
  experienceLevelsList: BindModel [];
  selectedSkill: BindModel;
  skillsList: BindModel [] ;
  startDate: Date;
  endDate: Date;
  requiredNumber : number;
  requiredSkillsList: RequiredSkill[] = [];
  loggedUser : User;
  skillsForm: FormGroup;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];
  touch: boolean;
  
  constructor(
    private projectService: ProjectService,
    //private adapter: DateAdapter<any>,
    private notificationService : NotificationService,
    private dateTimeService : DateTimeService,
    private pagerService: PagerService
  ) { }

  ngOnInit() {
    this.getSkillsDDList();
    this.getRequiredSkillsList();
    //this.adapter.setLocale('fr');
  }

  onRequiredNumberChange(value : any){
    if (value != "" && value < 1)
    {
      this.notificationService.warning("Invalide value");
      setTimeout(() => {
        this.requiredNumber = null;
      }, 100);
    }
  }

  selectDateEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    if (this.endDate != undefined && this.startDate != undefined) {
      if (this.startDate > this.endDate) {
        this.notificationService.warning("End Date must greater than Start Date");
        if (type == "start") {
          this.startDate = null;
        } else {
          this.startDate = null;
        }
      }
    }
  }

  getRequiredSkillsList(){
    this.projectService.GetRequiredSkillsList(this.PifId).subscribe(
      data => {
        this.requiredSkillsList = data as RequiredSkill[];
        this.setPage(1);
      },
      error => {
        this.notificationService.danger(error);
      }
    );
  }

  getSkillsDDList(){
    this.loggedUser = JSON.parse(localStorage.getItem('currentUser'));
    this.projectService.GetSkillsDDLists(this.loggedUser.activityId).subscribe(
      data => {
        this.skillsList = data.skillsList;
        this.experienceLevelsList = data.experienceLevelsList;
      },
      error => {
        this.notificationService.danger(error);
      }
    );
  }
 
  addRequiredSkill(formIsValid: boolean, form: FormGroup){
    if (formIsValid)
    {
      let rs = new RequiredSkill(this.PifId,
                                 this.selectedSkill,
                                 this.selectedExperienceLevel,
                                 this.requiredNumber,
                                 this.startDate,
                                 this.endDate
                                );
      this.projectService.AddRequiredSkillToPif(rs).subscribe(
        data => {
          rs.requiredSkillId = data;
          this.requiredSkillsList.push(rs);
          form.reset();
          this.setPage(1);
        },
        error => {
          this.notificationService.danger(error);
        }
      );
    }
    else{
      this.notificationService.danger("Please fill all required fields.")
    }
    //this.clearComponent();
  }

  removeRequiredSkill(requiredSkillId: number){
    this.projectService.RemoveRequiredSkill(requiredSkillId).subscribe(
      data => {
        this.requiredSkillsList = this.requiredSkillsList.filter(rs => rs.requiredSkillId != requiredSkillId);
        this.setPage(1);
      },
      error => {
        this.notificationService.danger(error);
      }
    );
  }

  clearComponent() {
    this.selectedSkill = null;
    this.startDate = null;
    this.endDate = null;
    this.requiredNumber = null;
    this.selectedExperienceLevel = null;
  }

  setPage(page: number) {
    if (page < 1 ) {
      return;
    }

    // get pager object from service
    this.pager = this.pagerService.getPager(this.requiredSkillsList.length, page);

    // get current page of items
    this.pagedItems = this.requiredSkillsList.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
}
