import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, Data } from '@angular/router';
//import { FormControl } from '@angular/forms';
import { ProjectService, NotificationService } from 'app/services';
import { DDListModel } from 'app/models/manageProject/DDList.model';
import { PIFModel } from 'app/models/manageProject';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { map } from 'rxjs/operators/map';
import { Observable } from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {animate, style, transition, trigger} from '@angular/animations';
import {FormControl, Validators, NgForm} from '@angular/forms';
import {NotificationsService} from 'angular2-notifications';
import { CanComponentDeactivate } from '../../../../guards/can-deactivate-guard';


@Component({
  selector: 'app-add',

  templateUrl: './createProject.component.html',
  styleUrls: ['./createProject.component.scss'],
  animations: [
          trigger('fadeInOutTranslate', [
            transition(':enter', [
              style({opacity: 0}),
              animate('400ms ease-in-out', style({opacity: 1}))
            ]),
            transition(':leave', [
              style({transform: 'translate(0)'}),
              animate('400ms ease-in-out', style({opacity: 0}))
            ])
          ])
        ]
})

export class CreateProjectComponent implements OnInit, CanComponentDeactivate {
  public user: any;
  public pif: PIFModel = new PIFModel();
  public DDLists: DDListModel[];
  public ClientDDList: DDListModel[];
  public CategoryDDList: DDListModel[];
  public ActivityDDList: DDListModel[];
  public TechnoDDList: DDListModel[];
  isEdit: boolean = false;
  disableEdit : boolean = false;
  InProgress: boolean = false;
  id: number;
  touch: boolean;

  @ViewChild('techSelect') groupsSelect: any;
  @ViewChild('pifDetailsForm') detailsForm : NgForm;

  constructor(
    private _avRoute: ActivatedRoute,
    private _router: Router,
    private projectService: ProjectService,
    private notificationService: NotificationService) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentUser'));

    //resolvers used to load pif details, in case of edit mode, and dd list content before loading the componenet.
    this._avRoute.data
    .subscribe(
      (data: Data) => {
        if (data['pif'] != null)
        {
          this.pif = data['pif'];
          this.groupsSelect.value = this.pif.TechIds;
          this.isEdit = true;
          this.id = this.pif.ID_PIF;

          this.disableEdit = this.pif.PIF_STATUS != 'Creation in progress';
        };

        if (data['ddList'] != null && data['ddList'] != undefined)
        {
          this.ActivityDDList = data['ddList'].filter(act => act.CODE_NAME === 'SI_ACTIVITY');
          this.ClientDDList = data['ddList'].filter(cli => cli.CODE_NAME === 'GP_CLIENT');
          this.CategoryDDList = data['ddList'].filter(cat => cat.CODE_NAME === 'GP_PROJECT_CATEGORY');
          this.TechnoDDList = data['ddList'].filter(tech => tech.CODE_NAME === 'GP_FUNCTIONPOINT');
        }
      },
      (error) =>{
        this.notificationService.danger(error);
      }
    );

    // this.detailsForm.valueChanges
    // .subscribe(
    //   (data: Data) => {
    //     console.log(data);
    //   }
    // );
  }

  //canDeactivate guard is used to control the exit of the screen when some data is not saved
  canDeactivate():Observable<boolean> | Promise<boolean> | boolean
  {
    if (this.detailsForm.dirty && !this.detailsForm.submitted)
    {
      this.notificationService.warning("Please save changes before exit.");
      return false;
    }
    else 
      return true;
  }

  Back() {
    this._router.navigate(['manageProjects/projects/new-projects']);
  };

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    if (this.pif.DATE_FINP != undefined && this.pif.DATE_DEBUTP != undefined) {
      if (this.pif.DATE_DEBUTP > this.pif.DATE_FINP) {
        this.notificationService.warning("End date must be greater than Start date");
        if (type == "start") {
          this.pif.DATE_DEBUTP = null;
        } else {
          this.pif.DATE_FINP = null;
        }
      }
    }
   }

  save() {
    if (this.detailsForm.valid) {
      this.InProgress = true;
      this.pif.ID_USER3 = this.user.UserId;
      this.projectService.SavePIFDetails(this.pif).subscribe(
        data => {
          this.InProgress = false;
          if (data != null) {
            if (data != 0) {
              this.pif.ID_PIF = data;
              this.notificationService.success('The project [ ' + this.pif.PROJECT_NAME + ' ]' + ' was saved successfully.');
            } else {
              this.notificationService.danger('An error occurred while saving the entries.');
            }
          }
        },
        error => {
          this.InProgress = false;
          this.notificationService.danger(error);
        }
      );
    } else {
      this.notificationService.warning('Please fill all required fields');
    }
  };

  ProjectStartDateChange(event) {

    this.pif.DATE_DEBUTP = event;
  }

  ProjectEndDateChange(event) {

    this.pif.DATE_FINP = event;
  }

  pifCost = new FormControl('', [Validators.required, Validators.min(0)]);

  getPifCostErrorMessage(){
    return this.pifCost.hasError('required') ? 'You must enter a value' :
        this.pifCost.hasError('min') ? 'Invalid value' :
            '';
  }
}
