
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'ProjectIndex-component',
  templateUrl: './index.view.html'
})
export class ProjectIndexComponent implements OnInit {

  constructor(private router: Router) { }
  ngOnInit() {

  }

  DisplayAddView() {
    this.router.navigate(['/projects/create']);
  }
}
