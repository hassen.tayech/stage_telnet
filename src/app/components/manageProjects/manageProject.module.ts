import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule, SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';

import { TimeSheetComponent } from 'app/components/manageProjects/timeSheets/timeSheet.component';
import { AddNewTaskComponent } from 'app/components/manageProjects/timeSheets/AddNewTask.component';


import { ProjectIndexComponent } from 'app/components/manageProjects/Projects/index.component';
import { ManageProjectRoutes } from 'app/components/manageProjects/manageProject-routing.module';
import { CreateProjectComponent } from './Projects/create/createProject.component';
import { ViewAllProjectsComponent } from './Projects/view-all/viewAllProjects.component';

import { ProjectService, NotificationService, DateTimeService, PagerService, WbsDisplayHandlerService } from 'app/services';
import { TeamComponent } from './Projects/create/tabs/team/team.component';
import { SkillsComponent } from './Projects/create/tabs/skills/skills.component';
import { ValidationProjectComponent } from './Projects/create/tabs/validation/validationProjet.component';
import { ViewNewProjectsComponent } from './Projects/view-new/viewNewProjects.component';
import { DocsComponent } from './Projects/create/tabs/docs/docs.component';
import { FileSelectDirective } from 'ng2-file-upload';

import { GenericService } from 'app/services/GenericService';
import { customHttpProvider } from 'app/helpers';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
//import { CUSTOM_DT_FORMATS } from 'app/helpers/dateTime-formats';

import {MatDialogModule} from "@angular/material";
import {SelectOptionService} from 'app/shared/elements/select-option.service';
import { IonRangeSliderModule } from "ng2-ion-range-slider";

import * as _moment from 'moment';

//import { ColorDirective } from './Projects/wbs/directive/color.directive';
import {SelectModule} from 'ng-select';
import { EditProjectComponent } from './Projects/edit/editProject.component';

//WBS components
import { addEditWBSComponent } from './Projects/edit/tabs/wbs/addEditWBS/addEditWBS.component';
import { OrgChartComponent } from './Projects/edit/tabs/wbs/org/org-chart.component';
import { WbsActivityColorDirective } from './Projects/edit/tabs/wbs/directive/wbs-activity-color.directive';
import { wbsComponent } from './Projects/edit/tabs/wbs/wbs.component';
import { DisplayActivityComponent } from './Projects/edit/tabs/wbs/display/display.component';
import { ActivityComponent } from './Projects/edit/tabs/wbs/activity/activity.component';
import { CanDeactivateGuard } from '../../guards/can-deactivate-guard';
import { PifResolver, PifDDListResolver } from 'app/resolvers/pif-resolver.service';
import { ProjectResolver, ProjectDDListResolver } from '../../resolvers/project-resolver.service';
import { TaskEditComponent } from 'app/components/manageProjects/timeSheets/TaskEdit/TaskEdit.component';
import { ProjectListComponent } from 'app/components/manageProjects/timeSheets/ProjectList/ProjectList.component';
import { expandCollapse } from 'app/animations';

const moment = _moment;

export const CUSTOM_DT_FORMATS = {
  parse: {
    dateInput: 'L',
  },
  display: {
    dateInput: 'L',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ManageProjectRoutes),
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    MatDialogModule,
    SelectModule,
    IonRangeSliderModule,
    //expandCollapse
  ],
  exports: [],
  declarations: [
    TimeSheetComponent,
    AddNewTaskComponent,
    addEditWBSComponent,
    ProjectIndexComponent,
    ViewAllProjectsComponent,
    TeamComponent,
    CreateProjectComponent,
    EditProjectComponent,
    SkillsComponent,
    ViewNewProjectsComponent,
    DocsComponent,
    FileSelectDirective,
    wbsComponent,
    OrgChartComponent,
    DisplayActivityComponent ,
    ActivityComponent,
    ValidationProjectComponent,
    WbsActivityColorDirective,
    TaskEditComponent,
    ProjectListComponent
  ],
  entryComponents: [
    AddNewTaskComponent,
    addEditWBSComponent,
    DisplayActivityComponent,
    TaskEditComponent,
    ProjectListComponent
  ],
  providers: [
    customHttpProvider,
    NotificationService,
    ProjectService,
    GenericService,
    DateTimeService,
    PagerService,
    SelectOptionService,
    {provide: MAT_DATE_LOCALE, useValue: 'fr-FR'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: CUSTOM_DT_FORMATS},
    PifResolver,
    PifDDListResolver,
    ProjectResolver,
    ProjectDDListResolver,
    WbsDisplayHandlerService
  ],
})
export class ManageProjectModule { }
