import { Component, OnInit, ElementRef } from '@angular/core';
import { AuthenticationService, UserService, NotificationService } from 'app/services';
import { User, UserProfile } from '../../../models/user';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private authService: AuthenticationService,
    private element: ElementRef,
    private notificationService: NotificationService
  ) { }

  userNumber: string="";
  userPassword: string="";
  returnUrl: string;

  ngOnInit() {
    document.querySelector('body').setAttribute('themebg-pattern', 'theme1');

    this.authService.logout();

    // get return url from route parameters or default to 'dashboard'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || 'dashboard';
  }

  login() {
    //Only for test, to be removed later
    if (this.userNumber =="tt" && this.userPassword == "tt")
    {
      var user : User = new User();
      user.UserName = "TelnetTeam";

      var profile: UserProfile = new UserProfile();
      profile.access_token="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ";
      var expires_in="2020/01/01";
      localStorage.setItem('expires_in', expires_in);
      profile.currentUser = user;
      //this.userService.setProfile(profile);
      this.router.navigate(['dashboard/default']);
      return;
    }
    //end

      this.authService.login(this.userNumber, this.userPassword)
      .subscribe(
          data => {
              if (data && this.userService.isAuthenticated()) {
                  this.router.navigate([this.returnUrl]);
              } else {
                  
                this.notificationService.danger("Login failed. Your login is invalid.");
              }
          },
          error => {
              this.notificationService.danger(error);
          }
      );
  }

}
