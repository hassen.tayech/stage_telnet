import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule }   from '@angular/forms';
import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-routing.module';
import { SharedModule } from 'app/shared/shared.module';
import { customHttpProvider } from 'app/helpers';
import { NotificationService } from 'app/services';


@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    SharedModule,
    //HttpClientModule,
    FormsModule,
    HttpModule
  ],
  declarations: [LoginComponent],
  providers:[
    //HttpClient,
    customHttpProvider,
    //HttpClient,
    NotificationService
  ]
})
export class LoginModule { }
