import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
//import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { MaterialModule } from './material.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminComponent } from './layout/admin/admin.component';
import { AuthComponent } from './layout/auth/auth.component';
import {SharedModule} from './shared/shared.module';
import {MenuItems} from './shared/menu-items/menu-items';
import {BreadcrumbsComponent} from './layout/admin/breadcrumbs/breadcrumbs.component';
import { AuthGuard } from './guards';
import { customHttpProvider, JwtHelper } from './helpers';
//import { AuthRoutingModule } from 'app/components/auth/auth-routing.module';
import { NotificationService, ShowErrorsService } from './services';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { CanDeactivateGuard } from './guards/can-deactivate-guard';
import { SimpleNotificationsModule, NotificationsService } from 'angular2-notifications/dist';
import { HassenComponent } from './hassen/hassen.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { InvoiceComponent } from './hassen/invoice/invoice.component';
import { ActivityPipe } from './pipes/activity.pipe';
import { StatusPipe } from './pipes/status.pipe';
import { ProjectNamePipe } from './pipes/projectName.pipe';
import { SearchProjectsService } from './services/searchProjects.service';
import { PaginatorComponent } from './hassen/paginator/paginator.component';
import { PaginatorPipe } from './pipes/paginator.pipe';
import { TableComponent } from './hassen/table/table.component';
import { StringsearchPipe } from './pipes/stringsearch.pipe';
import { SortPipe } from './pipes/sort.pipe';
import { DaterangeComponent } from './hassen/daterange/daterange.component';
import { Daterange2Component } from './hassen/daterange2/daterange2.component';
import { DaterangePipe } from './pipes/daterange.pipe';
import { Daterange2Pipe } from './pipes/daterange2.pipe';
import { ColorPickerModule } from '../../node_modules/ngx-color-picker';
import {AnimationService, AnimatorModule} from 'css-animator';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    AuthComponent,
    BreadcrumbsComponent,
    PageNotFoundComponent,
    HassenComponent,
    InvoiceComponent,
    ActivityPipe,
    StatusPipe,
    ProjectNamePipe,
    PaginatorComponent,
    PaginatorPipe,
    TableComponent,
    StringsearchPipe,
    SortPipe,
    DaterangeComponent,
    Daterange2Component,
    DaterangePipe,
    Daterange2Pipe

  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BrowserAnimationsModule,
    SimpleNotificationsModule.forRoot(),
    AppRoutingModule,
    SharedModule,
    NgMultiSelectDropDownModule.forRoot(),
    MaterialModule,
    ColorPickerModule,
    AnimatorModule
    
  ],
  providers: [
    customHttpProvider,
    NotificationsService,
    NotificationService,
    MenuItems,
    AuthGuard,
    CanDeactivateGuard,
    ShowErrorsService,
    SearchProjectsService,
    AnimationService
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
