import { Injectable } from "@angular/core";
import { ConnectionBackend, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response, Http, Headers } from "@angular/http";
import { HttpClient, HttpResponse } from '@angular/common/http';

import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { environment } from '../../environments/environment';
import { UserService, NotificationService } from 'app/services';
import { Route } from "@angular/router/src/config";
import { Router } from "@angular/router/src/router";
import { error } from "selenium-webdriver";

@Injectable()
export class CustomHttp extends Http {
  [x: string]: any;
  constructor(
    backend: ConnectionBackend,
    defaultOptions: RequestOptions
  ) {
    super(backend, defaultOptions);
  }
  private userService = new UserService();
  
  get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    if (this.userService.isAuthenticatedButTokenExpired())
      return Observable.throw("Your session has expired. Please log in again.");
    if (url.indexOf('i18n') >= 0) {
      return super.get(environment.localUrl + url).catch(this.handleError);
    } else {
      return super.get(environment.apiUrl + url, this.addJwt(options)).catch(this.handleError);
    }
  }

  post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
    if (this.userService.isAuthenticatedButTokenExpired())
      return Observable.throw("Your session has expired. Please log in again.");

    return super.post(environment.apiUrl + url, body, this.addJwt(options)).catch(this.handleError);
  }

  put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
    if (this.userService.isAuthenticatedButTokenExpired())
      return Observable.throw("Your session has expired. Please log in again.");

    return super.put(environment.apiUrl + url, body, this.addJwt(options)).catch(this.handleError);
  }

  delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    if (this.userService.isAuthenticatedButTokenExpired())
      return Observable.throw("Your session has expired. Please log in again.");

    return super.delete(environment.apiUrl + url, this.addJwt(options)).catch(this.handleError);
  }

  // private helper methods

  private addJwt(options?: RequestOptionsArgs): RequestOptionsArgs {
    // ensure request options and headers are not null
    var userService = new UserService();
    options = options || new RequestOptions();
    options.headers = options.headers || new Headers();

    // add authorization header with jwt token if user is authenticated and token is not expired
    if (userService.isAuthenticated()) {
      var userProfile = userService.getProfile()
      options.headers.append('Authorization', 'Bearer ' + userProfile.access_token);
    }

    return options;
  }

  private handleError(error: any) {
    return Observable.throw(error);
  }
}

export function customHttpFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions): Http {
  return new CustomHttp(xhrBackend, requestOptions);
}

export let customHttpProvider = {
  provide: Http,
  useFactory: customHttpFactory,
  deps: [XHRBackend, RequestOptions]
};
