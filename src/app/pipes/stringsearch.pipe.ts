import { Pipe, PipeTransform } from '@angular/core';
import { ProjectSearchResponse } from '../models/manageProject/project-search-response.model';
import { SearchProjectsService } from '../services/searchProjects.service';

@Pipe({
  name: 'stringsearch'
})
export class StringsearchPipe implements PipeTransform {


  tab: ProjectSearchResponse[] = [];

  transform(value: ProjectSearchResponse[], stringsearch: string): any {

    stringsearch = stringsearch.trim().toLowerCase();

    if (stringsearch.length !== null) {
      this.tab = [];
      for (let v of value) {

        if (v.PROJECT_NAME.trim().toLowerCase().search(stringsearch) !== -1
          || v.ACTIVITY_NAME.trim().toLowerCase().search(stringsearch) !== -1
          || v.PROJECT_STATUS.trim().toLowerCase().search(stringsearch) !== -1
          || v.PROJECT_DESCRIPTION.trim().toLowerCase().search(stringsearch) !== -1
          || v.CLIENT_NAME.trim().toLowerCase().search(stringsearch) !== -1) {
          if (v.PROJECT_DESCRIPTION.length > 100) {
            v.PROJECT_DESCRIPTION = v.PROJECT_DESCRIPTION.substr(0, 100) + ' ...';
          }
          this.tab.push(v);

        }

      }

      return this.tab;
    }
    return value;

  }

}