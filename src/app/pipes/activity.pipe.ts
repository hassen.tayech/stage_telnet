import { Pipe, PipeTransform } from '@angular/core';
import { ProjectSearchResponse } from '../models/manageProject/project-search-response.model';
import { SearchProjectsService } from '../services/searchProjects.service';

@Pipe({
  name: 'activity'
})
export class ActivityPipe implements PipeTransform {
  // constructor(public searchProjectsService: SearchProjectsService){}

  tab: ProjectSearchResponse[] = [];

  transform(value: ProjectSearchResponse[], selectedActivities: string[]): any {

    if (selectedActivities.length !== 0) {
      this.tab = [];
      for (let v of value) {
        if (selectedActivities.indexOf(v.ACTIVITY_NAME) !== -1) {
          this.tab.push(v);
        }
      }

      return this.tab;
    }
    return value;

  }
}