import { Pipe, PipeTransform } from '@angular/core';
import { ProjectSearchResponse } from '../models/manageProject/project-search-response.model';
import { SearchProjectsService } from '../services/searchProjects.service';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  constructor(public searchProjectsService: SearchProjectsService) { }

  transform(value: ProjectSearchResponse[], critaire: string, tri: number): any {  //tri = 1 si A-Z // -1 si Z-A

    return value.slice().sort(this.searchProjectsService.compare(critaire, tri));

  }

}