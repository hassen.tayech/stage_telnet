import { Pipe, PipeTransform } from '@angular/core';
import { ProjectSearchResponse } from '../models/manageProject/project-search-response.model';
import { SearchProjectsService } from '../services/searchProjects.service';
import { NgbDateStruct } from '../../../node_modules/@ng-bootstrap/ng-bootstrap';

@Pipe({
  name: 'daterange'
})
export class DaterangePipe implements PipeTransform {
  constructor(public searchProjectsService: SearchProjectsService) { }

  tab: ProjectSearchResponse[] = [];

  transform(value: ProjectSearchResponse[], from: NgbDateStruct, to: NgbDateStruct): any {

    if (from !== null && to === null) {
      this.tab = [];
      for (let v of value) {
        var ngbDateStruct = { day: v.PROJECT_REALSTARTDATE.getUTCDate(), month: v.PROJECT_REALSTARTDATE.getUTCMonth() + 1, year: v.PROJECT_REALSTARTDATE.getUTCFullYear() };
        if (after(ngbDateStruct, from) || equals(ngbDateStruct, from)) {        
          this.tab.push(v);
        }
      }
      return this.tab;
    }
    else if (from === null && to !== null) {
      
      this.tab = [];
      for (let v of value) {
        var ngbDateStruct = { day: v.PROJECT_REALSTARTDATE.getUTCDate(), month: v.PROJECT_REALSTARTDATE.getUTCMonth() + 1, year: v.PROJECT_REALSTARTDATE.getUTCFullYear() };
        if (before(ngbDateStruct, to) || equals(ngbDateStruct, to)) {
          this.tab.push(v);
        }
      }
      return this.tab;
    }
    else if (from !== null && to !== null) {
      
      this.tab = [];
      for (let v of value) {
        var ngbDateStruct = { day: v.PROJECT_REALSTARTDATE.getUTCDate(), month: v.PROJECT_REALSTARTDATE.getUTCMonth() + 1, year: v.PROJECT_REALSTARTDATE.getUTCFullYear() };
       if ((after(ngbDateStruct, from) && after(to, ngbDateStruct)) || equals(ngbDateStruct, from) || equals(to, ngbDateStruct)) {
          this.tab.push(v);
        }
      }
      return this.tab;
    }
    return value;

  }
}

const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day > two.day : one.month > two.month : one.year > two.year;
