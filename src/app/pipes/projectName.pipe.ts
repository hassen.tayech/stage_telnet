import { Pipe, PipeTransform } from '@angular/core';
import { ProjectSearchResponse } from 'app/models/manageProject/project-search-response.model';
import { SearchProjectsService } from '../services/searchProjects.service';

@Pipe({
  name: 'projectName'
})

export class ProjectNamePipe implements PipeTransform {
  constructor(public searchProjectsService: SearchProjectsService) { }


  tab: ProjectSearchResponse[] = [];
  transform(value: ProjectSearchResponse[], selectedProjectNames: string[]): any {

    if (selectedProjectNames.length !== 0) {
      this.tab = [];

      for (let v of value) {
        if (selectedProjectNames.indexOf(v.PROJECT_NAME) !== -1) {
          this.tab.push(v);
        }
      }

      this.searchProjectsService.resultChanged.next(this.tab.length);

      this.searchProjectsService.showingData = this.tab;
      return this.tab;
    }

    this.searchProjectsService.resultChanged.next(value.length);

    this.searchProjectsService.showingData = value;
    return value;

  }
}