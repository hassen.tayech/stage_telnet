import { Pipe, PipeTransform } from '@angular/core';
import { ProjectSearchResponse } from 'app/models/manageProject/project-search-response.model';
import { SearchProjectsService } from '../services/searchProjects.service';

@Pipe({
  name: 'status'
})
export class StatusPipe implements PipeTransform {

  tab: ProjectSearchResponse[] = [];

  transform(value: ProjectSearchResponse[], selectedStatus: string[]): any {

    if (selectedStatus.length !== 0) {
      this.tab = [];
      for (let v of value) {
        if (selectedStatus.indexOf(v.PROJECT_STATUS) !== -1) {
          this.tab.push(v);
        }
      }
      return this.tab;
    }
    return value;

  }
}