import { Pipe, PipeTransform } from '@angular/core';
import { ProjectSearchResponse } from '../models/manageProject/project-search-response.model';
import { SearchProjectsService } from '../services/searchProjects.service';

@Pipe({
  name: 'paginator'
})
export class PaginatorPipe implements PipeTransform {

  constructor(public searchProjectsService: SearchProjectsService) { }

  startIndex: number;
  endIndex: number;
  filterValue: string;
  transform(value: ProjectSearchResponse[], pageIndex: number, pageSize: number): any {



    this.startIndex = (pageIndex * pageSize) + 1;
    this.endIndex = pageSize * (pageIndex + 1);

    this.searchProjectsService.showingDataLastFilter = value.slice(this.startIndex - 1, this.endIndex);

    this.searchProjectsService.resultChanged2.next();

  

    return value.slice(this.startIndex - 1, this.endIndex);
  }

}
