import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  short_label?: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: 'TelnetTeam',
    main: [
      {
        state: '',
        short_label: 'H',
        name: 'Home',
        type: 'link',
        icon: 'icon-home'
      },
      {
        state: 'manageProjects',
        short_label: 'P',
        name: 'Project Management',
        type: 'sub',
        icon: 'icon-briefcase',
        children: [
          {
            state: 'projects',
            name: 'Projects',
            type: 'sub',
            children: [
              {
                state: 'new-projects',
                name: 'New Project',
                target: true
              },
              {
                state: 'all-projects',
                name: 'Projects List',
                target: true
              }
            ]
          }, {
            state: 'timesheet',
            name: 'TimeSheet',
            target: true
          }, {
            state: '',
            name: 'Reporting',
            type: 'link',
          }
        ]
      },
      {
        state: '',
        short_label: 'HR',
        name: 'Human Resource',
        type: 'sub',
        icon: 'icon-user',
        children: []
      },
      {
        state: '',
        short_label: 'P',
        name: 'Purchase',
        type: 'sub',
        icon: 'icon-shopping-cart',
        children: []
      },
      {
        state: '',
        short_label: 'ER',
        name: 'E-Recruit',
        type: 'sub',
        icon: 'icon-id-badge',
        children: []
      },
      {
        state: '',
        short_label: 'HD',
        name: 'Help Desk',
        type: 'sub',
        icon: 'icon-support',
        children: []
      },
      {
        state: '',
        short_label: 'R',
        name: 'Reservation',
        type: 'sub',
        icon: 'icon-calendar',
        children: []
      }
    ]
  }
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }
}
