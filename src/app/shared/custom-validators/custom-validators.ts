import { FormControl, FormGroup, ValidationErrors, AbstractControl, ValidatorFn } from '@angular/forms';
import { Injectable } from '@angular/core';
import { DateTimeService } from 'app/services';

@Injectable()
export class CustomValidators {
  static dateTimeService = new DateTimeService();

  static numeric(c: FormControl): ValidationErrors {
    const numValue = Number(c.value);
    const isValid = !isNaN(numValue);
    const message = {
      'numeric': {
        'message': 'Only numeric values are acecpted'
      }
    };
    return isValid ? null : message;
  }

  // static posNumber(c: FormControl): ValidationErrors {
  //   const numValue = Number(c.value);
  //   const isValid = !isNaN(numValue) && numValue >= 0;
  //   const message = {
  //     'posNumber': {
  //       'message': 'Invalid value'
  //     }
  //   };
  //   return isValid ? null : message;
  // }

  static checkDates(d1: FormControl, d2: FormControl, source: string): ValidationErrors {
    const startDate = d1.value;
    const endDate = d2.value;
    var message = {}
    const isValid = CustomValidators.dateTimeService.checkFirstAndLastDateOrder(startDate, endDate);
    if (!isValid){
      if (source="start"){
        message = {
          'startDateInvalid': {
            'message': 'Start Date must be lower than End Date.'
          }
        };
        //startDate.setErrors({'startDateInvalid': true});
      }
      else {
        message = {
          'endDateInvalid': {
            'message': 'End Date must be greater than Start Date.'
          }
        };
        //endDate.setErrors({'endDateInvalid': true});
      }
    }

    return isValid ? null : message;
  }

//   static dateLessThan(dateField1: string, dateField2: string, validatorField: { [key: string]: boolean }): ValidatorFn {
//     return (c: AbstractControl): { [key: string]: boolean } | null => {
//         const date1 = c.get(dateField1).value;
//         const date2 = c.get(dateField2).value;
//         if ((date1 !== null && date2 !== null) && date1 > date2) {
//             return validatorField;
//         }
//         return null;
//     };
// }



}