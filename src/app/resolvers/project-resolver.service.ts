import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { ProjectModel, DDListModel } from "app/models/manageProject";
import { Observer } from "rxjs/Observer";
import { Observable } from "rxjs/observable";
import { ProjectService } from "app/services";
import { Injectable } from "@angular/core";


@Injectable()
export class ProjectResolver implements Resolve<ProjectModel>{

    constructor(private projectService: ProjectService){}
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Observable<ProjectModel> | Promise<ProjectModel> | ProjectModel
    {
        if (route.params['id'] != "")
            return this.projectService.GetProjectDetails(+route.params['id']);
        else
            return null;
    }
}

@Injectable()
export class ProjectDDListResolver implements Resolve<DDListModel[]>{

    constructor(private projectService: ProjectService){}
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Observable<DDListModel[]> | Promise<DDListModel[]> | DDListModel[]
    {
        return this.projectService.GetDDLists();
    }
}