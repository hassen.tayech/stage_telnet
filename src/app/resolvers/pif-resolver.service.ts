import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { PIFModel, DDListModel } from "app/models/manageProject";
import { Observer } from "rxjs/Observer";
import { Observable } from "rxjs/observable";
import { ProjectService } from "app/services";
import { Injectable } from "@angular/core";


@Injectable()
export class PifResolver implements Resolve<PIFModel>{

    constructor(private projectService: ProjectService){}
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Observable<PIFModel> | Promise<PIFModel> | PIFModel
    {
        if (route.params['id'] != "")
            return this.projectService.GetPIFDetails(+route.params['id']);
        else
            return null;
    }
}

@Injectable()
export class PifDDListResolver implements Resolve<DDListModel[]>{

    constructor(private projectService: ProjectService){}
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Observable<DDListModel[]> | Promise<DDListModel[]> | DDListModel[]
    {
        return this.projectService.GetDDLists();
    }
}