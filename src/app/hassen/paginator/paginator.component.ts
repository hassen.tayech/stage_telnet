import { Component, OnInit, Input, AfterViewChecked, AfterContentChecked, ViewChild } from '@angular/core';
import { SearchProjectsService } from '../../services/searchProjects.service';
import { Subscription } from '../../../../node_modules/rxjs/Subscription';
import { MatPaginator } from '../../../../node_modules/@angular/material';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public searchProjectsService: SearchProjectsService) { }

  subscription: Subscription;
  length: number;


  ngOnInit() {

    this.length = this.searchProjectsService.getProjects().length;

    this.subscription = this.searchProjectsService.resultChanged
      .subscribe(
        (showingProjectsNumber: number) => {
          this.length = showingProjectsNumber;
          this.searchProjectsService.pageIndex = 0;
          this.paginator.pageIndex = 0;

        }
      );
  }

  onPaginateChange(event) {

    this.searchProjectsService.pageIndex = event.pageIndex;
    this.searchProjectsService.pageSize = event.pageSize;

  }

  displayedColumns = ['position', 'name', 'weight', 'symbol'];

}
