import { Component, OnInit, DoCheck, AfterViewInit, OnChanges, AfterContentInit, AfterContentChecked, AfterViewChecked } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { SearchProjectsService } from '../../services/searchProjects.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  constructor(public searchProjectsService: SearchProjectsService) { }
  ngOnInit() {

    this.dataSource = new MatTableDataSource(this.searchProjectsService.showingDataLastFilter);

    this.searchProjectsService.resultChanged2
      .subscribe(
        () => {
          this.dataSource = new MatTableDataSource(this.searchProjectsService.showingDataLastFilter);
        }
      );

  }

  displayedColumns = ['PROJECT_NAME', 'PROJECT_STATUS', 'ACTIVITY_NAME', 'CLIENT_NAME', 'buttons'];

  dataSource = new MatTableDataSource(this.searchProjectsService.getProjects());



}
