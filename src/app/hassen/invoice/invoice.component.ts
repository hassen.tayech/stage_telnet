import { Component, OnInit, Input } from '@angular/core';
import { ProjectSearchResponse } from '../../models/manageProject/project-search-response.model';
import { AnimationBuilder, AnimationService } from 'css-animator';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})

export class InvoiceComponent implements OnInit {

  @Input() project: ProjectSearchResponse;


  ngOnInit() {

  }

}

