import { Component, OnInit } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { SearchProjectsService } from '../../services/searchProjects.service';

@Component({
  selector: 'app-daterange2',
  templateUrl: './daterange2.component.html',
  styleUrls: ['./daterange2.component.scss']
})
export class Daterange2Component implements OnInit {


  fromDate_endDate = new Date();
  toDate_endDate = new Date();

  constructor(public searchProjectsService: SearchProjectsService) { }

  ngOnInit() {

    this.fromDate_endDate = null;
    this.toDate_endDate = null;

    this.searchProjectsService.resetDate2
      .subscribe(
        () => {
          this.fromDate_endDate = null;
          this.toDate_endDate = null;
        }
      );
  }



  onPicker21() {
    this.searchProjectsService.fromDate_endDate = {
      day: this.fromDate_endDate.getUTCDate(),
      month: this.fromDate_endDate.getUTCMonth() + 1,
      year: this.fromDate_endDate.getUTCFullYear()
    };
  }

  onPicker22() {
    this.searchProjectsService.toDate_endDate = {
      day: this.toDate_endDate.getUTCDate(),
      month: this.toDate_endDate.getUTCMonth() + 1,
      year: this.toDate_endDate.getUTCFullYear()
    };
  }
}