import { Component, OnInit } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { SearchProjectsService } from '../../services/searchProjects.service';

@Component({
  selector: 'app-daterange',
  templateUrl: './daterange.component.html',
  styleUrls: ['./daterange.component.scss']
})
export class DaterangeComponent implements OnInit {


  fromDate_startDate = new Date();
  toDate_startDate = new Date();

  // fromDate_endDate = new Date();
  // toDate_endDate = new Date();

  constructor(public searchProjectsService: SearchProjectsService) { }

  ngOnInit() {

    this.fromDate_startDate = null;
    this.toDate_startDate = null;

    // this.fromDate_endDate = null;
    // this.toDate_endDate = null;

    this.searchProjectsService.resetDate
      .subscribe(
        () => {
          this.fromDate_startDate = null;
          this.toDate_startDate = null;

          // this.fromDate_endDate = null;
          // this.toDate_endDate = null;
        }
      );
  }

  onPicker11() {
    this.searchProjectsService.fromDate_startDate = {
      day: this.fromDate_startDate.getUTCDate(),
      month: this.fromDate_startDate.getUTCMonth() + 1,
      year: this.fromDate_startDate.getUTCFullYear()
    };
  }

  onPicker12() {
    this.searchProjectsService.toDate_startDate = {
      day: this.toDate_startDate.getUTCDate(),
      month: this.toDate_startDate.getUTCMonth() + 1,
      year: this.toDate_startDate.getUTCFullYear()
    };
  }

  // onPicker21() {
  //   this.searchProjectsService.fromDate_endDate = {
  //     day: this.fromDate_endDate.getUTCDate(),
  //     month: this.fromDate_endDate.getUTCMonth() + 1,
  //     year: this.fromDate_endDate.getUTCFullYear()
  //   };
  // }

  // onPicker22() {
  //   this.searchProjectsService.toDate_startDate = {
  //     day: this.toDate_endDate.getUTCDate(),
  //     month: this.toDate_endDate.getUTCMonth() + 1,
  //     year: this.toDate_endDate.getUTCFullYear()
  //   };
  // }
}