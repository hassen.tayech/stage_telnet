import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, Input, AfterViewChecked } from '@angular/core';
import { SearchProjectsService } from '../services/searchProjects.service';
import { NgbDateParserFormatter } from '../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { AnimationBuilder, AnimationService } from 'css-animator';

@Component({
  selector: 'app-hassen',
  templateUrl: './hassen.component.html',
  styleUrls: ['./hassen.component.scss',
    '../../scss/animation.scss'
  ],
  encapsulation: ViewEncapsulation.None
})

export class HassenComponent implements OnInit {


  constructor(public searchProjectsService: SearchProjectsService,
    public parserFormatter: NgbDateParserFormatter,
    animationService: AnimationService) {
    this.animator = animationService.builder();
    this.animator.useVisibility = true;
  }

  @ViewChild('toAnimate') toAnimate: ElementRef;//to animate list view
  @ViewChild('toAnimate2') toAnimate2: ElementRef;//to animate grid view

  emptyResult: boolean = false;


  ngOnInit() {
    ////after pagination pipe subscribe
    this.searchProjectsService.resultChanged2
      .subscribe(
        () => {
          this.animate(this.toAnimate.nativeElement, 'bounceIn', false);
          this.animate(this.toAnimate2.nativeElement, 'bounceIn', false);

          //Message for empty result
          /* lors de l'affichage du block de empty result, ça fonctionne avec un erreur
          cette valeur change en activant le subscribe "resultChanged2" dans le dernier filtre

          voir ce lien : https://blog.angular-university.io/angular-debugging/
          */  
          if(this.searchProjectsService.showingDataLastFilter.length === 0){
            this.emptyResult = true;
          }else{
            this.emptyResult = false;
          }
           
          //search box
          if(this.searchInput===''){
            this.filterBySearchBox = false;
          }else{
            this.filterBySearchBox = true;
          }

          //ProjectName
          if(this.selectedProjectNames.length === 0 ){
            this.filterByProjectName = false;
          }else{
            this.filterByProjectName = true;
          }

          //Activity
          if(this.selectedActivities.length===0){
            this.filterByActivity = false;
          }else{
            this.filterByActivity = true;
          }

          //status
          if(this.selectedStatus.length===0){
            this.filterByStatus = false;
          }else{
            this.filterByStatus = true;
          }
          
          //date range
          if(this.searchProjectsService.fromDate_startDate !== null ||
            this.searchProjectsService.toDate_startDate !== null){
            this.filterByDateRange = true;
          }else{
            this.filterByDateRange = false;
          }

          //date range2
          if(this.searchProjectsService.fromDate_endDate !== null  ||
            this.searchProjectsService.toDate_endDate !== null){
              console.log('okkééé');
            this.filterByDateRange2 = true;
          }else{
            this.filterByDateRange2 = false;
          }

          //all filters
          if(this.filterBySearchBox === true || this.filterByProjectName === true 
            || this.filterByActivity === true || this.filterByStatus === true 
            || this.filterByDateRange === true){
            this.filterByAnyFilter = true;
          }else{
            this.filterByAnyFilter = false;
          }

        }
      );

    // multiselect settings
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,//limiter l'affichage des options choisies.
      allowSearchFilter: true,
      maxHeight: 150
    };

    //get project names to show it in it's multiselect
    for (let p of this.searchProjectsService.getProjects()) {
      this.projectNames.push(p.PROJECT_NAME);
    }
  }

  ///////////////filter props start//////////////////////

  filterBySearchBox: boolean = false;
  filterByProjectName: boolean = false;
  filterByActivity: boolean = false;
  filterByStatus: boolean = false;
  filterByDateRange: boolean = false;
  filterByDateRange2: boolean = false;
  filterByAnyFilter: boolean = false;
  
  searchInput: string = '';
  listview: boolean = true;
  selectedSortBy = 'Project Name';
  allProjects = this.searchProjectsService.getProjects();

  selectedActivities: string[] = [];
  selectedStatus: string[] = [];
  selectedProjectNames: string[] = [];
  sortByValues = ['Project Name', 'Activity', 'Status','Start Date','End Date'];

  allActivities: string[] = this.searchProjectsService.getActivities();
  allStatus = this.searchProjectsService.getStatus();

  projectNames = [];
  dropdownSettings = {};
  ///////////////filter props end//////////////////////



  //////////START/RESET/Active Filters Buttons start////////////////
  sortUp: boolean = true;
  sort: number = 1;
  //sortButtonName:string = 'A - Z'
  onSortButton() {
    this.sortUp = !this.sortUp;
    if (this.sortUp) this.sort = 1;
    else this.sort = -1;

  }

  onResetProjectName() {
    this.selectedProjectNames = [];
  }
  onResetActivity() {
    this.selectedActivities = [];
  }
  onResetStatus() {
    this.selectedStatus = [];
  }

  onResetDateRange() {
    this.searchProjectsService.fromDate_startDate = null;
    this.searchProjectsService.toDate_startDate = null;

    this.searchProjectsService.resetDate.next();
  }

  onResetDateRange2() {
    this.searchProjectsService.fromDate_endDate = null;
    this.searchProjectsService.toDate_endDate = null;

    this.searchProjectsService.resetDate2.next();
  }

  onResetSearchBox(){
    this.searchInput = '';
  }

  onResetAllFilters(){
    this.onResetProjectName();
    this.onResetActivity();
    this.onResetStatus();
    this.onResetDateRange();
    this.onResetSearchBox();
  }
  //////////START/RESET/Active Filters Buttons end////////////////


  ///////////View Mode Start///////////////
  onViewModeList() {
    this.listview = true;
    this.animate(this.toAnimate.nativeElement, 'bounceIn', false);
  }
  onViewModeGrid() {
    this.listview = false;
    this.animate(this.toAnimate2.nativeElement, 'bounceIn', false);
  }
  ///////////View Mode end///////////////


  ///////////animation////////////
  public animation = 'fadeIn';
  public isVisible = true;
  public isAnimating = false;

  public animator: AnimationBuilder;

  public animate(element: HTMLElement, animation: string, status: boolean) {
    if (status) {
      this.animation = animation;
    }
    this.isAnimating = true;

    this.animator
      .setType(this.animation)
      .setDuration(1500)
      .animate(element)
      .then(() => {
        this.isAnimating = false;
      })
      .catch(e => {
        this.isAnimating = false;
        console.log('css-animator: Animation aborted', e);
      });
  }
}
