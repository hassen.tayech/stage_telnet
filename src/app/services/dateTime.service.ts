import { Injectable } from '@angular/core';
import * as moment from 'moment';

const localCode = "fr";


@Injectable()
export class DateTimeService {
  nullDate : Date = new Date("0001-01-01T00:00:00+01:00");
  startOfWeek = moment().startOf('isoWeek');
  endOfWeek = moment().endOf('isoWeek');

  isNullDate(date): boolean{
    return date == null || date == undefined || date == this.nullDate;
  }

  getShortFormat(date : Date):string{
    if (date == null || date == undefined)
      return "";
    else
      return moment(date).locale(localCode).format("L");
  }

  FormatDateToString(dateToFormat: any): string {
    return dateToFormat.isValid() ? dateToFormat.locale(localCode).format("L") : "";
  }

  GetDate(dateToFormat: string): string {
    return moment(new Date(dateToFormat)).isValid() ? moment(new Date(dateToFormat)).locale(localCode).format("L") : "";
  }

  ConvertDateToNumber(dateToFormat): number {
    return moment(dateToFormat).valueOf();
  }
  
  getLastDate(day1, day2){

    if (this.isNullDate(day1))
      return day2;
  
    if (this.isNullDate(day2))
      return day1;

    //Initiallize variables
    var d1 = moment(day1);
    var d2 = moment(day2);
    
    // if((d1.dayOfYear() === d2.dayOfYear()) && (d1.year() === d2.year())){
    //   return d1;
    // }

    if(d1.isBefore(d2))
      return day2;
    else
      return day1;
  }

  getFirstDate(day1, day2){

    if (this.isNullDate(day1))
      return day2;

    if (this.isNullDate(day2))
      return day1;

    //Initiallize variables
    var d1 = moment(day1);
    var d2 = moment(day2);
    
    // if((d1.dayOfYear() === d2.dayOfYear()) && (d1.year() === d2.year())){
    //   return d1;
    // }

    if(d1.isAfter(d2))
      return day2;
    else
      return day1;
  }
  
  today(){
    var today=  new Date();
    today.setHours(0,0,0,0);
    return today;
  }

  calculateBusinessDays(startDate, endDate, lastDayIncluded=true){
    //Initiallize variables
    var day1 = moment(startDate);
    var day2 = moment(endDate);
    var adjust = lastDayIncluded ? 1 : 0;
    
    if((day1.dayOfYear() === day2.dayOfYear()) && (day1.year() === day2.year())){
      return 0;
    }
    
    //Check if second date is before first date to switch
    if(day2.isBefore(day1)){
      day2 = moment(startDate);
      day1 = moment(endDate);
    }
  
    //Check if first date starts on weekends
    if(day1.day() === 6) { //Saturday
      //Move date to next week monday
      day1.day(8);
    } else if(day1.day() === 0) { //Sunday
      //Move date to current week monday
      day1.day(1);
    }
  
    //Check if second date starts on weekends
    if(day2.day() === 6) { //Saturday
      //Move date to current week friday
      day2.day(5);
    } else if(day2.day() === 0) { //Sunday
      //Move date to previous week friday
      day2.day(-2);
    }
  
    var day1Week = day1.week();
    var day2Week = day2.week();
  
    //Check if two dates are in different week of the year
    if(day1Week !== day2Week){
      //Check if second date's year is different from first date's year
      if (day2Week < day1Week){
        day2Week += day1Week;
      }
      //Calculate adjust value to be substracted from difference between two dates
      adjust = -2 * (day2Week - day1Week) + 1;
    }
  
    return day2.diff(day1, 'days') + adjust;
  }

  checkFirstAndLastDateOrder(firstDate, lastDate){
    firstDate = moment(firstDate);
    lastDate = moment(lastDate);
    //return true if lastDate is equal or after firstDate
    // if((firstDate.dayOfYear() === lastDate.dayOfYear()) && (firstDate.year() === lastDate.year()))
    //   return true;
    // else
    return lastDate.isAfter(firstDate);
  }

  areEqualDates(date1, date2){
    date1 = moment(date1);
    date2 = moment(date2);

    return ((date1.dayOfYear() === date2.dayOfYear()) && (date1.year() === date2.year()));
     
  }

  getCurrentWeekDays(){
    var day = this.startOfWeek;
    var days=[];
    while (day <= this.endOfWeek) {
        days.push(this.FormatDateToString(day));
        //days.push(day.format("L").toString());
        day = day.clone().add(1, 'd');
    }
    return(days);
  }

  getPreviousWeek(){
    this.startOfWeek = this.startOfWeek.weekday(-7);
    this.endOfWeek = this.endOfWeek.weekday(-7);
    return(this.getCurrentWeekDays());
  }

  getNextWeek(){
    this.startOfWeek = this.startOfWeek.weekday(7);
    this.endOfWeek = this.endOfWeek.weekday(7);
    return(this.getCurrentWeekDays());
 }

//  getWeek(startDate, endDate){
//   var day = startDate;
//   var days=[];
//   while (day <= endDate) {
//     days.push(this.FormatDateToString(day));
//     day = day.clone().add(1, 'd');
//   }
//   console.log(days);
//   return(days);
// }


}
