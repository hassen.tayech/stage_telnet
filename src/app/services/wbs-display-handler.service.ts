import { ElementRef } from "@angular/core";
import { Activity } from "../models/manageProject/activity.model";

export class WbsDisplayHandlerService{
    wbsContainer : ElementRef;
    public zoomValue : number = 100;
    zoomMin: number = 30;
    zoomMax: number = 150;
    zoomStep: number = 10;
  
    innerWidth: any;
    navBarWidth : number = 235;
    wbsCardWidh : number = 180;
    wbsCardHeight : number = 153;
    wbsCardMargin : number = 15;
    mouseIsDown :boolean = false;
    dragOffset = [0, 0];
    initialWbsContainerOffset = [ 0, 0];
    topActivity : Activity;

 
    init(_wbsContainer : ElementRef, _topActivity: Activity){
        this.wbsContainer = _wbsContainer;
        this.initialWbsContainerOffset = [this.wbsContainer.nativeElement.offsetLeft-40, this.wbsContainer.nativeElement.offsetTop-40];
        this.innerWidth = window.innerWidth;
        this.topActivity = _topActivity;
        this.zoomValue = 100;
    }
    
    getWbsWidth(): number{
        let columnWidth: number = this.wbsCardWidh + 2 * this.wbsCardMargin;
        let wbsWidth = this.topActivity.columnNumberDisplayed * columnWidth;
        return wbsWidth;
    }

    getWbsHeight(): number{
        let columnHeight: number = this.wbsCardHeight;
        let wbsHeight = this.topActivity.maxLevelDisplayed * columnHeight;
        return wbsHeight;
    }

    zoomAuto(innerWidth = this.innerWidth){
        //let columnWidth: number = this.wbsCardWidh + 2 * this.wbsCardMargin;
        let wbsWidth = this.getWbsWidth();//this.topActivity.columnNumberDisplayed * columnWidth;
        let wbsContainerWidth = innerWidth - this.navBarWidth * 1.5;
        
        if (wbsWidth <= wbsContainerWidth || wbsWidth == 0)
            this.zoomValue = 100;
        else 
            this.zoomValue = Math.max((wbsContainerWidth / wbsWidth) * 100, this.zoomMin);
    }
      
    zoomIn(){
        this.zoomValue = Math.min(this.zoomValue + this.zoomStep, this.zoomMax);
    }
      
    zoomOut(){
        this.zoomValue = Math.max(this.zoomValue - this.zoomStep, this.zoomMin);
    }

    resetWbsDisplay(){
        this.zoomAuto();
        this.wbsContainer.nativeElement.style.left = this.initialWbsContainerOffset[0] + 'px';
        this.wbsContainer.nativeElement.style.top  = this.initialWbsContainerOffset[1] + 'px';
    }

    mousedown($event){
        this.mouseIsDown = true;
        this.dragOffset = [ 
            this.wbsContainer.nativeElement.offsetLeft - $event.clientX,
            this.wbsContainer.nativeElement.offsetTop - $event.clientY
        ]
      }
    
      mouseup($event){
        this.mouseIsDown = false;
        this.wbsContainer.nativeElement.style.cursor = "auto";
      }
    
      mousemove($event){
        $event.preventDefault();
        if (this.mouseIsDown){
          var mousePosition = {
              x : $event.clientX,
              y : $event.clientY
          };
    
          var leftPosition = Math.min((mousePosition.x + this.dragOffset[0]), this.initialWbsContainerOffset[0] + this.getWbsWidth() * this.zoomValue/100);
          var topPosition = Math.min((mousePosition.y + this.dragOffset[1]), this.initialWbsContainerOffset[1] + this.getWbsHeight() * this.zoomValue/100);
          
          //this.wbsContainer.nativeElement.style.left = leftPosition + 'px';
          //this.wbsContainer.nativeElement.style.top  = topPosition + 'px';
          
          //this.wbsContainer.nativeElement.style.cursor = "move";
        }
      }
    
    dbClickOnWbsContainer(){

        // this.zoomValue += 20;
        // if(this.zoomValue >= this.zoomMax)
        //     this.zoomAuto();
    }
}