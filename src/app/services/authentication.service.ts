﻿import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import { UserService } from '../services/user.service';

@Injectable()
export class AuthenticationService {
    constructor(
        private http: Http,
        private userService: UserService
    ) { }

    login(userNumber: string, userPassword: string) {
        return this.http.post('/Login/Authenticate', { userNumber: userNumber, userPassword: userPassword })    
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let profile = response.json();
                if (profile && profile.access_token) {
                    this.userService.setProfile(profile);
                }
                return profile;
            });
    }

    logout() {
        // remove user's profile from local storage to log user out
        this.userService.resetProfile();
    }
}