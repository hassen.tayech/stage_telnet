﻿import { Injectable } from '@angular/core';
import {JwtHelper} from 'app/helpers/jwt-helper';
import { UserProfile } from 'app/models/user';

@Injectable()
export class UserService {
    constructor(
        ) { }

        private jwtHelper: JwtHelper = new JwtHelper();

        isAuthenticated() {
            let profile = this.getProfile();
            var validToken = profile.access_token != "" && profile.access_token != null;
            var isTokenExpired = this.isTokenExpired(profile);
            return validToken && !isTokenExpired;
        }

        isAuthenticatedButTokenExpired() {
            let profile = this.getProfile();
            var validToken = profile.access_token != "" && profile.access_token != null;
            var isTokenExpired = this.isTokenExpired(profile);
            return validToken && isTokenExpired;
        }

        isAuthorized() {
            let profile = this.getProfile();
            var validToken = profile.access_token != "" && profile.access_token != null;
            var isTokenExpired = this.isTokenExpired(profile);
            return validToken && !isTokenExpired;
        }

        isTokenExpired(profile: UserProfile) {
            var expiration = new Date(profile.expires_in)
            return expiration < new Date();
        }
        
        setProfile(profile: UserProfile){
            if (profile && profile.access_token && (profile.access_token != ""))
            {
                var expires_in = this.jwtHelper.getTokenExpirationDate(profile.access_token).toString();
                localStorage.setItem('access_token', profile.access_token);
                localStorage.setItem('expires_in', expires_in);
                localStorage.setItem('currentUser', JSON.stringify(profile.currentUser));
            }
        }
        
        getProfile(): UserProfile{
            var accessToken = localStorage.getItem('access_token');
            var userProfile: UserProfile = new UserProfile();
            if (accessToken) {
                userProfile.access_token = accessToken;
                userProfile.expires_in = localStorage.getItem('expires_in');
                if (userProfile.currentUser == null) {
                    // var user: User = new User();
                    // user = JSON.parse(localStorage.getItem('currentUser'));
                    userProfile.currentUser = JSON.parse(localStorage.getItem('currentUser'));
                }
            }
            return userProfile;
        }
    
        resetProfile() {
            localStorage.removeItem('access_token');
            localStorage.removeItem('expires_in');
            localStorage.removeItem('currentUser');
        }

        getRole(): string {
            return 'PROJECT_MANAGER';
        }

        getPermissions(): string[]{
            //var profile = this.getProfile();

            return ['ADD_PROJECT2','EDIT_PROJECT','VIEW_PROJECT'];

        }
}
