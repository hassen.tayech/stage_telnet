﻿import { Injectable } from '@angular/core';
import { Subject } from '../../../node_modules/rxjs/Subject';
import { ProjectSearchResponse } from '../models/manageProject/project-search-response.model';
import { NgbDateStruct } from '../../../node_modules/@ng-bootstrap/ng-bootstrap';

//@Injectable()
export class SearchProjectsService {

  ///////dates
  fromDate_startDate: NgbDateStruct = null;
  toDate_startDate: NgbDateStruct = null;

  fromDate_endDate: NgbDateStruct = null;
  toDate_endDate: NgbDateStruct = null;

  //////////Paginator/////////
  //   showingProjectsNumber:number=this.getProjects().length;
  resultChanged = new Subject<number>();
  resultChanged2 = new Subject<void>();
  resetDate = new Subject<void>();
  resetDate2 = new Subject<void>();

  pageIndex: number = 0;
  pageSize: number = 10;

  showingData: ProjectSearchResponse[] = [];
  showingDataLastFilter: ProjectSearchResponse[] = [];

  constructor(
  ) { }


  /////////////Combobox Activity///////////////
  activities = ['Automobile', 'DataBox', 'Dect', 'Defence', 'Multimedia',
    'PLM', 'Security', 'Telecom', 'Information System'
  ];

  getActivities() {
    return this.activities;
  }

  /////////////Combobox Status///////////////

  private status = ['Open', 'In Progress', 'Blocked', 'Closed', 'Planned'
  ];

  getStatus() {
    return this.status;
  }


  private projects: ProjectSearchResponse[] = [
    new ProjectSearchResponse(
      'PROJECT_ID1', 'PROJECT_NAMEA', 'PROJECT_ABBREVIATION1', 'PROJECT_DESCRIPTION1 PROJECT_DESCRIPTION1 PROJECT_DESCRIPTION1 PROJECT DESCRIPTION1 PROJECT_DESCRIPTION1 PROJECT DESCRIPTION1', new Date(2000, 0, 2), new Date(2015, 0, 7),
      'Closed', 'STATUS1', 'PROJECT_COST1', 'PROJECT_REFERENCE1', 'ACTIVITY_ID1', 'Automobile', 'CLIENT_ID1', 'CLIENT_NAME1'
    ),
    new ProjectSearchResponse(
      'PROJECT_ID2', 'PROJECT_NAMEB', 'PROJECT_ABBREVIATION2', 'PROJECT_DESCRIPTION2', new Date(2001, 0, 8), new Date(2016, 0, 8),
      'In Progress', 'STATUS2', 'PROJECT_COST2', 'PROJECT_REFERENCE2', 'ACTIVITY_ID2', 'Information System', 'CLIENT_ID2', 'CLIENT_NAME2'
    ),
    new ProjectSearchResponse(
      'PROJECT_ID3', 'PROJECT_NAMEC', 'PROJECT_ABBREVIATION3', 'PROJECT_DESCRIPTION3', new Date(2002, 0, 8), new Date(2017, 0, 8),
      'Closed', 'STATUS3', 'PROJECT_COST3', 'PROJECT_REFERENCE3', 'ACTIVITY_ID3', 'Security', 'CLIENT_ID3', 'CLIENT_NAME3'
    ), new ProjectSearchResponse(
      'PROJECT_ID1', 'PROJECT_NAMED', 'PROJECT_ABBREVIATION1', 'PROJECT_DESCRIPTION1', new Date(2003, 0, 8), new Date(2018, 7, 8),
      'Planned', 'STATUS1', 'PROJECT_COST1', 'PROJECT_REFERENCE1', 'ACTIVITY_ID1', 'Automobile', 'CLIENT_ID1', 'CLIENT_NAME1'
    ),
    new ProjectSearchResponse(
      'PROJECT_A', 'PROJECT_NAMEE', 'PROJECT_ABBREVIATION2', 'PROJECT_DESCRIPTION2', new Date(2004, 5, 8), new Date(2019, 1, 8),
      'In Progress', 'STATUS2', 'PROJECT_COST2', 'PROJECT_REFERENCE2', 'ACTIVITY_ID2', 'Information System', 'CLIENT_ID2', 'CLIENT_NAME2'
    ),
    new ProjectSearchResponse(
      'PROJECT_B', 'PROJECT_NAMEF', 'PROJECT_ABBREVIATION3', 'PROJECT_DESCRIPTION3', new Date(2005, 6, 8), new Date(2020, 5, 8),
      'Planned', 'STATUS3', 'PROJECT_COST3', 'PROJECT_REFERENCE3', 'ACTIVITY_ID3', 'Security', 'CLIENT_ID3', 'CLIENT_NAME3'
    )
    ,
    new ProjectSearchResponse(
      'PROJECT_C', 'PROJECT_NAMEG', 'PROJECT_ABBREVIATION1', 'PROJECT_DESCRIPTION1', new Date(2006, 1, 8), new Date(2021, 5, 8),
      'Closed', 'STATUS1', 'PROJECT_COST1', 'PROJECT_REFERENCE1', 'ACTIVITY_ID1', 'Automobile', 'CLIENT_ID1', 'CLIENT_NAME1'
    ),
    new ProjectSearchResponse(
      'PROJECT_D', 'PROJECT_NAMEH', 'PROJECT_ABBREVIATION2', 'PROJECT_DESCRIPTION2', new Date(2007, 11, 8), new Date(2022, 9, 8),
      'In Progress', 'STATUS2', 'PROJECT_COST2', 'PROJECT_REFERENCE2', 'ACTIVITY_ID2', 'Information System', 'CLIENT_ID2', 'CLIENT_NAME2'
    ),
    new ProjectSearchResponse(
      'PROJECT_ID3', 'PROJECT_NAMEI', 'PROJECT_ABBREVIATION3', 'PROJECT_DESCRIPTION3', new Date(2008, 0, 8), new Date(2023, 10, 8),
      'Closed', 'STATUS3', 'PROJECT_COST3', 'PROJECT_REFERENCE3', 'ACTIVITY_ID3', 'Security', 'CLIENT_ID3', 'CLIENT_NAME3'
    ), new ProjectSearchResponse(
      'PROJECT_ID1', 'PROJECT_NAMEJ', 'PROJECT_ABBREVIATION1', 'PROJECT_DESCRIPTION1', new Date(2009, 0, 8), new Date(2024, 9, 8),
      'Planned', 'STATUS1', 'PROJECT_COST1', 'PROJECT_REFERENCE1', 'ACTIVITY_ID1', 'Automobile', 'CLIENT_ID1', 'CLIENT_NAME1'
    ),
    new ProjectSearchResponse(
      'PROJECT_ID2', 'PROJECT_NAMEK', 'PROJECT_ABBREVIATION2', 'PROJECT_DESCRIPTION2', new Date(2010, 1, 1), new Date(2025, 2, 2),
      'In Progress', 'STATUS2', 'PROJECT_COST2', 'PROJECT_REFERENCE2', 'ACTIVITY_ID2', 'Information System', 'CLIENT_ID2', 'CLIENT_NAME2'
    ),
    new ProjectSearchResponse(
      'PROJECT_ID3', 'PROJECT_NAMEL', 'PROJECT_ABBREVIATION3', 'PROJECT_DESCRIPTION3', new Date(2011, 4, 9), new Date(2026, 10, 10),
      'Planned', 'STATUS3', 'PROJECT_COST3', 'PROJECT_REFERENCE3', 'ACTIVITY_ID3', 'Security', 'CLIENT_ID3', 'CLIENT_NAME3'
    )
  ];

  getProjects() {
    return this.projects;
  }

  compare(compareBy: string, tri: number) {  //tri = 1 si A-Z // -1 si Z-A
    let chaine1: string;
    let chaine2: string;
    let date1 : NgbDateStruct;
    let date2 : NgbDateStruct;
    return function (a: ProjectSearchResponse, b: ProjectSearchResponse) {
      // chaine1:string;
      if (compareBy === 'Activity') {
        chaine1 = a.ACTIVITY_NAME;
        chaine2 = b.ACTIVITY_NAME;
      } else if (compareBy === 'Status') {
        chaine1 = a.PROJECT_STATUS;
        chaine2 = b.PROJECT_STATUS;
      } else if (compareBy === 'Project Name') {
        chaine1 = a.PROJECT_NAME;
        chaine2 = b.PROJECT_NAME;
      } else if (compareBy === 'Start Date') {
        date1 = { day: a.PROJECT_REALSTARTDATE.getUTCDate(), month: a.PROJECT_REALSTARTDATE.getUTCMonth() + 1, year: a.PROJECT_REALSTARTDATE.getUTCFullYear() };
        date2 = { day: b.PROJECT_REALSTARTDATE.getUTCDate(), month: b.PROJECT_REALSTARTDATE.getUTCMonth() + 1, year: b.PROJECT_REALSTARTDATE.getUTCFullYear() };
      } else if (compareBy === 'End Date') {
        date1 = { day: a.PROJECT_REALENDDATE.getUTCDate(), month: a.PROJECT_REALENDDATE.getUTCMonth() + 1, year: a.PROJECT_REALENDDATE.getUTCFullYear() };
       date2 = { day: b.PROJECT_REALENDDATE.getUTCDate(), month: b.PROJECT_REALENDDATE.getUTCMonth() + 1, year: b.PROJECT_REALENDDATE.getUTCFullYear() };
      }

      let comparison = 0;

      if (compareBy === 'Activity' || compareBy === 'Status' || compareBy === 'Project Name') {
        chaine1 = chaine1.toUpperCase();
        chaine2 = chaine2.toUpperCase();
        if (chaine1 > chaine2) {
          comparison = 1;
        } else if (chaine1 < chaine2) {
          comparison = -1;
        }
      } else if  (compareBy === 'Start Date' || compareBy === 'End Date'){
      //  console.log('before func =  ' + before(date1,date2));    
          if(before(date2,date1)){
            comparison = 1;
          }else if (before(date1,date2)) {
            comparison = -1;
          }
      }
    
      return comparison * tri;
    }

  }

}

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day < two.day : one.month < two.month : one.year < two.year;

    const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one && two && two.year === one.year && two.month === one.month && two.day === one.day;