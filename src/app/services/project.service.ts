import { Injectable, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { GenericService } from 'app/services/GenericService';
import { PIFModel, Member,Doc, RequiredSkill, ProjectModel } from 'app/models/manageProject';
import { Activity } from 'app/models/manageProject/activity.model';
import { Assignement } from 'app/models/manageProject/assignement.model';


import { userProject } from 'app/models/manageProject/timesheet';

@Injectable()
export class ProjectService {
 

  constructor( private genericService: GenericService) {}

  ActivityDeleted = new Subject<Activity>();
  ActivityAdded = new Subject<Activity>();

  ActivityDetailsChanged = new EventEmitter();
  refreshActivitiesSelected = new EventEmitter<userProject[]>();
  ActivitySelected = new EventEmitter<Activity>();
  

  SaveProjectDetails(project: ProjectModel): any {
      return this.genericService
          .createService('/Project/SaveProjectDetails', project);
  }

  GetUserProjectsList(userId: string, dataTablesParameters: any){
    return this.genericService
      .createService("/ProjectSearch/GetUserProjectsList/?user_id="+userId, dataTablesParameters);
  }

  GetAllProjectsList(dataTablesParameters: any){
    return this.genericService
      .createService("/ProjectSearch/GetAllProjectsList", dataTablesParameters);
  }

  GetProjectManagerPifList(pm_user_id: string) {
    return this.genericService
      .getServiceWithDynamicQueryTerm("/Project/GetProjectManagerPifList", "pm_user_id", pm_user_id);
  };

  //Get member by activity
  GetMemberListByActivity(activityId: string) {
    return this.genericService
      .getServiceWithDynamicQueryTerm('/Project/GetMembersList', "activityId", activityId);
  };
    // Get all roles
  GetAllRoles() {
    return this.genericService
      .getService('/Project/GetRoleList');
  };

  GetDDLists() {
    return this.genericService
      .getService('/GetProjectDDLists');
  };

  GetTypeEntriesList() {
    return this.genericService
      .getService('/GetTypeEntryList');
  };
  //Add member
  AddMember(member:Member) {
    return this.genericService.createService("/Project/InsertMember", member);
  }
  //Get PIF member list
  GetPifTeam(pif: string) {
    return this.genericService.getServiceWithDynamicQueryTerm("/Project/GetProjectTeamMember","PIF",pif);
  }

  GetPifSearchDDLists(){
    return this.genericService
    .getService('/Project/GetPifSearchDDLists');
  }

	GetPIFDetails(id: Number) {
		return this.genericService
			.getServiceWithDynamicQueryTerm('/GetPIFDetails', "pif_id", id.toString());
  }
  
  GetProjectDetails(id: Number) {
		return this.genericService
			.getServiceWithDynamicQueryTerm('/Project/GetProjectDetails', "projectId", id.toString());
	}

  saveDoc(docTosave: Doc) {
   return this.genericService
     .createService("/SaveDoc", docTosave);
  }

  GetListDoc(pifId: number) {
    return this.genericService
      .getServiceWithDynamicQueryTerm('/GetListDocByPifId', "pifId", pifId.toString());
  }

  DownloadFile(fileName: string) {
    return this.genericService
      .getServiceWithDynamicQueryTerm('/DownloadFile', "fileName", fileName);
  }

  RemoveEntry(docToDelete: Doc) {
    return this.genericService
      .createService('/RemoveEntry', docToDelete);
  }

  RemoveMember(memberToDelete: Member) {
    return this.genericService
      .createService('/RemoveMember', memberToDelete);
  }

	SavePIFDetails(pif: PIFModel) {
		return this.genericService
			.createService('/SavePIFDetails', pif);
  }
  
  GetUserPifList(userId: string, dataTablesParameters: any){
    return this.genericService
      .createService("/ProjectSearch/GetUserPifList/?user_id="+userId, dataTablesParameters);
  }

  AddRequiredSkillToPif(requiredSkil: RequiredSkill){
    return this.genericService
			.createService('/Project/AddRequiredSkillToPif', requiredSkil);
  }

  GetRequiredSkillsList(pifId: number){
      return this.genericService
        .getServiceWithDynamicQueryTerm('/Project/GetRequiredSkillsList', "pifId", pifId.toString());
  }

  GetSkillsDDLists(activityId: number) {
    return this.genericService
      .getServiceWithDynamicQueryTerm('/Project/GetSkillsDDLists', "activityId", activityId.toString());
  }

  RemoveRequiredSkill(requiredSkillId: number){
    return this.genericService
      .getServiceWithDynamicQueryTerm('/Project/RemoveRequiredSkill', "requiredSkillId", requiredSkillId.toString());
  }

  GetActivitiesByProjectId(projectId: string) {
    return this.genericService
      .getServiceWithDynamicQueryTerm('/Project/GetActivityDetailsByProjectId', "projectId", projectId);
  }

  GetActivityDetails(activityId: number) {
    return this.genericService
      .getServiceWithDynamicQueryTerm('/Project/GetActivityDetails', "activityId", activityId.toString());
  };


  saveActivity(activity: Activity) {
    return this.genericService
      .createService('/Project/SaveActivity', activity);
  }

  GetUsersByProjectId(projectId: number) {
    return this.genericService
      .getServiceWithDynamicQueryTerm('/Project/GetUsersByProjectId', "projectId", projectId.toString());
  };

  AddAssignements(assignements: Assignement) {
    return this.genericService
      .createService('/Project/InsertAssignement', assignements);
  };

  DeleteActivity(activityId: number){
    return this.genericService
      .getServiceWithDynamicQueryTerm('/Project/DeleteActivity', "activityId", activityId.toString());
  }
  
  GetUserActivity(userId: number) {
		return this.genericService
			.getServiceWithDynamicQueryTerm('/Project/GetUserActivity', "userId", userId.toString());
	}

}
