
import { Injectable } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';

@Injectable()
export class NotificationService {
  constructor(private servicePNotify: NotificationsService){}
  public options: any = {
    position: ['top', 'right'],
  };
 
 public info(mssg: any){
  this.addNotify(mssg, 'info', 'bg-c-purple small-icon');
}

public success(mssg: any){
  this.addNotify(mssg, 'success', 'bg-c-green small-icon');
}

public warning(mssg: any){
  this.addNotify(mssg, 'warning', 'bg-c-purple small-icon');
}

public danger(mssg: any){
  this.addNotify(mssg, 'error', 'bg-c-red small-icon');
}

addNotify(mssg: any, type, cssClass: string)
{
    this.servicePNotify.remove();
    this.options  = {
      position : ['top', 'right'],
      maxStack: 8,
      timeOut: 2000,
      showProgressBar: true,
      pauseOnHover: true,
      lastOnBottom: true,
      clickToClose: true,
      preventDuplicates: false,
      preventLastDuplicates: false,
      theClass: cssClass,
      rtl: false,
      animate: 'fromRight'
    };

    switch (type) {
      case 'success':
        this.options.timeOut = 500;
        this.servicePNotify.success('Success',mssg);
        break;
      case 'error':
        this.servicePNotify.error('Error',mssg);
        break;
      case 'warning':
        this.servicePNotify.create('Warning',mssg,'warn');
        break;
      case 'info':
        this.servicePNotify.info('Info',mssg);
        break;
    }
  }
}
