
export class Activity{
    public ACTIVITY_ID: string;
    public DEPARTMENT_ID: string;
    public ACTIVITY_NAME: string;
    public ACTIVITY_CODE: string;
}