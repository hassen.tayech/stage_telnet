import  {BindModel} from 'app/models/shared';
import {Observable} from 'rxjs/Observable';
import * as moment from 'moment';

export class RequiredSkill{
    constructor(
      public pifId: Number,
      public skill: BindModel,
      public experienceLevel: BindModel,
      public requiredNumber: Number,
      public startDate: Date,
      public endDate: Date)
    {  }
    public requiredSkillId: Number;
}