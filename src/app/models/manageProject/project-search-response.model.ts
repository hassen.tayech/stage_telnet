export class ProjectSearchResponse {
    public PROJECT_ID: string;
    public PROJECT_NAME: string;
    public PROJECT_ABBREVIATION: string;
    public PROJECT_DESCRIPTION: string;
    public PROJECT_REALSTARTDATE: Date;
    public PROJECT_REALENDDATE: Date;
    public PROJECT_STATUS: string;
    public STATUS: string;
    public PROJECT_COST: string;
    public PROJECT_REFERENCE: string;
    public ACTIVITY_ID: string;
    public ACTIVITY_NAME: string;
    public CLIENT_ID: string;
    public CLIENT_NAME: string;

    constructor(
        PROJECT_ID: string,
        PROJECT_NAME: string,
        PROJECT_ABBREVIATION: string,
        PROJECT_DESCRIPTION: string,
        PROJECT_REALSTARTDATE: Date,
        PROJECT_REALENDDATE: Date,
        PROJECT_STATUS: string,
        STATUS: string,
        PROJECT_COST: string,
        PROJECT_REFERENCE: string,
        ACTIVITY_ID: string,
        ACTIVITY_NAME: string,
        CLIENT_ID: string,
        CLIENT_NAME: string
    ) {
        this.PROJECT_ID = PROJECT_ID;
        this.PROJECT_NAME = PROJECT_NAME;
        this.PROJECT_ABBREVIATION = PROJECT_ABBREVIATION;
        this.PROJECT_DESCRIPTION = PROJECT_DESCRIPTION;
        this.PROJECT_REALSTARTDATE = PROJECT_REALSTARTDATE;
        this.PROJECT_REALENDDATE = PROJECT_REALENDDATE;
        this.PROJECT_STATUS = PROJECT_STATUS;
        this.STATUS = STATUS;
        this.PROJECT_COST = PROJECT_COST;
        this.PROJECT_REFERENCE = PROJECT_REFERENCE;
        this.ACTIVITY_ID = ACTIVITY_ID;
        this.ACTIVITY_NAME = ACTIVITY_NAME;
        this.CLIENT_ID = CLIENT_ID;
        this.CLIENT_NAME = CLIENT_NAME;
    }
}

