export class DDListModel {
  public CODE_NAME: string;
  public CODE_VALUE: string;
  public DESCRIPTION: string;
  public MESSAGE_ID: string;
}
