
export class Doc {
  public id: number;
  public IdPif: number;
  public title: string;
  public reference: string;
  public typeId: number;
  public typeDesc: string;
  public fileName: string;
  public editionDate: Date;
  public filePath: string;
  public fileExtension: string
  constructor() {
  }
}
