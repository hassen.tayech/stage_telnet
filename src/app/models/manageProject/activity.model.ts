import { Assignement } from "./assignement.model";
import { Subject } from "rxjs/Subject";

export enum ActivityStatus{
  NotStarted,
  Started,
  Completed
}

export class Activity {
  public name: string;
  public description: string;
  public activityId: number;
  public projectId: number;
  public parentId: number;
  public activityNumber?: string;
  public plannedStartDate: Date;
  public plannedEndDate: Date;
  public assignements?: Assignement[];
  public hasChilds?: boolean;
  public hasParent? : boolean;
  public estimatedCharge: number = 0;
  public level?: number;
  public subActivities?: Activity[];  
  public showChilds?: boolean = true;
  public hasAssignement?: boolean = false;
  public advancement : number = 0;
  public columnNumber? : number = 0;
  public columnNumberDisplayed? : number = 0;
  public maxLevel? : number = 0;
  public maxLevelDisplayed? : number = 0;
  public plannedDuration: number = 0;
  public elapsedDuration?: number = 0;
  public actualCost: number = 0;
  public earnedValue?: number = 0;
  public spi?: number = 0;
  public cpi?: number = 0;
  public isSelected? : boolean = false;
  public realEndDate? : Date = null;
  public status? : ActivityStatus = ActivityStatus.NotStarted;
  
}

