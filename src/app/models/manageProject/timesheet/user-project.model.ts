import { userActivity } from "./user-activity.model";

export class userProject {

    PROJECT_ID: Number;
    PROJECT_NAME: string;
    PROJECT_ENDDATE: Date;
    PROJECT_STARTDATE: Date;
    PROJECT_GENERIC: Number;
    projectActivities: userActivity[] = [];
    public SELECT_DESELECT_PROJECT: Boolean;

}
