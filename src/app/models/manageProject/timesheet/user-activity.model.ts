//import { weektimeEntry } from "app/models/manageProject/timesheet/week-time-entry.model";

export class userActivity {

    public PROJECT_ID: Number;
    public PROJECT_NAME: string;
    public PROJECT_ENDDATE: Date;
    public PROJECT_STARTDATE: Date;
    public PROJECT_GENERIC: Number;
    public ASSIGNMENT_ENDDATE: Date;
    public ASSIGNMENT_STARTDATE: Date;
    public ACTIVITY_ID: Number;
    public ACTIVITY_NAME: string;
    public ACTIVITY_DESCRIPTION: string;
    public ACTIVITY_ENDDATE: Date;
    public ACTIVITY_STARTDATE: Date;
    public ACTIVITY_SELECTED: Boolean;
    
    //public userTimeEntry: weektimeEntry = new weektimeEntry();

    constructor(){
       // this.userTimeEntry = new weektimeEntry();
    }
}