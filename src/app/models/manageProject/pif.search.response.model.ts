export class PifSearchResponse {
    public ID_PIF : string;
    public PROJECT_NAME :string;
    public ACTIVITY_ID :string;
    public ACTIVITY_NAME :string
    public PM_ID :string;
    public PM_NAME :string;
    public VISA_PM :string;
    public DH_ID :string;
    public DH_NAME :string;
    public VISA_DH :string;
    public SQA_ID :string;
    public SQA_NAME :string;
    public VISA_SQA :string;
    public PIF_STATUS : string;
}