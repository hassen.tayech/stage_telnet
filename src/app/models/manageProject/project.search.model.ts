export class projectSearch {

  ProjectName: string;
  ProjectCode: string;
  ProjectStatus: string;
  ProjectCategory: string;
  ProjectActivity: string;
  ProjectStartDate: string;
  ProjectEndDate: string;
  ProjectClient: string;
  ProjectFiliale: string;
  ProjectCost: string;
  ProjectCreatedBy: string;
}


