import {User} from '.';

export class UserProfile {
    access_token: string;
    expires_in: string;
    currentUser: User;
}