﻿export class User {
    UserId: string;
    FirstName: string;
    LastName: string;
    UserName: string;
    USER_FULL_NAME: string;
    USER_ROLE:string;
    Image: string;
    activityId: number;
}
