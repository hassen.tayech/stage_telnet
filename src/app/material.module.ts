import { NgModule } from '@angular/core';

import {
    MatButtonModule,
    MatSelectModule,
    MatPaginatorModule,
    MatTableModule,
    MatInputModule,
    MatRadioModule,
    MatNativeDateModule,
    MatDatepickerModule
} from '@angular/material'



@NgModule({

    imports: [
        MatButtonModule,
        MatSelectModule,
        MatPaginatorModule,
        MatTableModule,
        MatInputModule,
        MatRadioModule,
        MatDatepickerModule,
        MatNativeDateModule//,
//        MatMomentDateModule
        
    ],
    exports: [
        MatButtonModule,
        MatSelectModule,
        MatPaginatorModule,
        MatTableModule,
        MatInputModule,
        MatRadioModule,
        MatDatepickerModule,
        MatNativeDateModule//,
       // MatMomentDateModule
    ],

})

export class MaterialModule { }